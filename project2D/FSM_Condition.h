#pragma once

class Agent;

class Condition
{
public:
	Condition();
	virtual ~Condition();

	virtual bool Test(Agent* agent) const = 0;
};





// Within range condition 
// Returns true when target is within range of the agent

class WithinRangeCondition : public Condition {

public:
	WithinRangeCondition(const Agent* agent, float range);

	virtual bool Test(Agent* agent) const override;

private:
	const Agent* m_Target;
	float m_RangeSqr;
};





class NotCondition : public Condition{
public:
	// Takes ownership of the input condition
	NotCondition(Condition* originalCondition);
	~NotCondition();

	virtual bool Test(Agent* agent) const override;

private:

	Condition* m_Condition;

};