#pragma once
#include "SpriteObject.h"
#include <string>

class WaterHole : public SpriteObject
{
public:

	WaterHole() { m_TagId = "WaterHole"; }
	WaterHole(std::string& tagId) { m_TagId = tagId; }

	WaterHole(Texture_Shr_Ptr& texture, const Vector2& pos, float collisionRadius, bool drawDebug = false)
		: SpriteObject(texture)
	{
		m_TagId = "WaterHole";
		m_Position = pos;
		m_Collider.radius = collisionRadius;
		m_DrawCollision = drawDebug;
	}

	WaterHole(Texture_Shr_Ptr& texture, float collisionRadius) 
		: SpriteObject(texture) 
	{
		m_TagId = "WaterHole";
		m_Collider.radius = collisionRadius;
	}	
	virtual ~WaterHole() {}

	void		 Drink() {}

};


class Tree : public SpriteObject
{
public:

	Tree() { m_TagId = "Tree"; }
	Tree(Texture_Shr_Ptr& texture, const Vector2& pos, float collisionRadius, bool drawDebug = false)
		: SpriteObject(texture)
	{ 
		m_TagId = "Tree"; 
		m_Position = pos;
		m_Collider.radius = collisionRadius;
		m_DrawCollision = drawDebug;
	}

	virtual ~Tree() {}

};