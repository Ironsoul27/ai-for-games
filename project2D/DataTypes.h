#pragma once
#include <unordered_map>
#include <string>
#include <vector>

class Agent;
class Flock;
class BlackboardData;

typedef std::unordered_map<std::string, std::vector<Agent*>> Agent_UOM;
typedef std::unordered_map<std::string, std::vector<Flock>&> Flock_Map;
typedef std::unordered_map<std::string, BlackboardData> BlackBoard_Map;