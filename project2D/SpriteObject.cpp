#include "SpriteObject.h"
#include <memory>



SpriteObject::SpriteObject()
{
}


SpriteObject::~SpriteObject()
{
}


void SpriteObject::onDraw(aie::Renderer2D * renderer)
{
	if (m_Texture != nullptr) {
		renderer->setRenderColour(1, 1, 1);
		renderer->drawSprite(m_Texture.get(), m_Position.x, m_Position.y, m_TextureWidth, m_TextureHeight);
	}
	// Null Texture
	else {
		renderer->setRenderColour(1, 0, 1);
		renderer->drawBox(m_Position.x, m_Position.y, 20, 20);
		renderer->setRenderColour(1, 1, 1);
	}
	
	// Draw Collision
	if (m_DrawCollision) {
		renderer->setRenderColour(1, 1, 1, 0.2f);
		renderer->drawCircle(m_Collider.center.x, m_Collider.center.y, m_Collider.radius);
		renderer->setRenderColour(1, 1, 1, 1.f);
	}
}