#include "FiniteStateMachine.h"
#include "FSM_State.h"
#include "FSM_Transition.h"
#include "FSM_Condition.h"
#include <assert.h>


FiniteStateMachine::FiniteStateMachine()
	: m_CurrentState(nullptr)
{}

FiniteStateMachine::~FiniteStateMachine() { 

	// Clean up all memory for states/conditions/transitions
	for (auto state : m_States)
		delete state;
}

bool FiniteStateMachine::Update(Agent * agent, float deltaTime)
{

	if (m_ScheduledState != nullptr)  // If our scheduled state has something lined up, then replace our current state with the scheduled one
	{
		// Give the old state a chance to cleanup, by calling exit()
		if (m_CurrentState != nullptr)
			m_CurrentState->Exit(agent);

		m_CurrentState = m_ScheduledState;
		// Initialise new state
		m_CurrentState->Init(agent);

		m_ScheduledState = nullptr;
	}

	if (m_CurrentState != nullptr) // If we have a state defined, then tell the state to do its processing.
	{
		Transition* transition = m_CurrentState->GetTriggeredTransition(agent);

		if (transition != nullptr)
		{
			// Give the old state a chance to cleanup, by calling exit()
			m_CurrentState->Exit(agent);

			// Change the curent state
			m_CurrentState = transition->GetTargetState();

			// Tell the new state that we have just entered it, and it can initialise
			m_CurrentState->Init(agent);
		}

		// Tell current state to do processing
		m_CurrentState->Update(agent, deltaTime);

		return true;
	}
	return false;
}

void FiniteStateMachine::Draw(Agent * agent, aie::Renderer2D * renderer)
{
	// FSM could do some other drawing here, if it wanted.

		// FSM tell the current state that it's time to draw.
		// The current state can choose to override the draw() function to provide specific implementation.

	assert(renderer != nullptr && "FSM -- Draw failed to pass through renderer");

	if (m_CurrentState != nullptr)
	{
		m_CurrentState->Draw(agent, renderer);
	}
}

State * FiniteStateMachine::AddState(State * state, bool makeCurrent)
{
	m_States.push_back(state);

	if (makeCurrent)
		SetScheduledState(state);

	return state;
}

void FiniteStateMachine::SetScheduledState(State * state)
{
	m_ScheduledState = state;
}

State * FiniteStateMachine::FindState(const std::string & name) const
{
	for (State* s : m_States)
	{
		if (s->GetName() == name)
			return s;
	}

	return nullptr;
}
