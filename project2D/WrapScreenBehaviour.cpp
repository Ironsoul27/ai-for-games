#include "WrapScreenBehaviour.h"
#include "Agent.h"
#include <Application.h>
#include <assert.h>


WrapScreenBehaviour::WrapScreenBehaviour(aie::Application* app)
	: m_App(app)
{

}


WrapScreenBehaviour::~WrapScreenBehaviour()
{
}

bool WrapScreenBehaviour::Update(Agent * agent, float deltaTime)
{
	if (agent == nullptr)
		return false;

	const Vector2& pos = agent->GetPosition();

	if (pos.x < 0) 
	{
		agent->SetPosition({ m_App->getWindowWidth() + pos.x, pos.y });
	}
	else if (pos.x > m_App->getWindowWidth())
	{
		agent->SetPosition({ pos.x - m_App->getWindowWidth(), pos.y });
	}

	if (pos.y < 0)
	{
		agent->SetPosition({ pos.x, m_App->getWindowHeight() + pos.y });
	}
	else if (pos.y > m_App->getWindowHeight())
	{
		agent->SetPosition({ pos.x, pos.y - m_App->getWindowHeight()});
	}

	return true;
}
