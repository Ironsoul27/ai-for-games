#pragma once

enum States
{
	Flee,
	SeekPrey,
	SeekTarget,
	Wander,
	PlayerControlled,
	FollowLeader,
	FindWaterHole,
	NodeTreeTarget,
	EatTarget
};
