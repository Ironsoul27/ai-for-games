#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "KeyboardBehaviour.h"
#include "SteeringBehaviour.h"
#include "DebugBehaviour.h"
#include "DragBehaviour.h"
#include "WrapScreenBehaviour.h"
#include "FollowLeaderBehaviour.h"
#include "ScreenEdgeRepelBehaviour.h"
#include "Pathfinding.h"
#include "BehaviourTree.h"
#include "Sphere.h"
#include "SceneObject.h"
#include "SpriteObject.h"
#include "SceneObjects.h"

#include "BT_Actions.h"
#include "BT_Conditions.h"

#include "Predator.h"
#include "Prey.h"

#include <time.h>
#include <sstream>
#include <cmath>
#include <ctgmath>


aie::Font* g_SystemFont;

bool Application2D::startup() {
	
	srand((unsigned int)time(NULL));


	m_2dRenderer = new aie::Renderer2D();

	m_font = new aie::Font("./font/consolas.ttf", 17);

	g_SystemFont = m_font;

	setBackgroundColour(.12f, .2f, .12f);

	Waterhole_tex = std::make_shared<aie::Texture>("../textures/waterhole.png");
	Tree_tex = std::make_shared<aie::Texture>("../textures/tree3.png");


	//<a href = "https://pngtree.com/free-vectors">free vectors from pngtree.com< / a>
	////////////////////////////////////////////////////////////////////////////////////

	// TODO

	// For pathfinding, have pathways around the map for moving to waterholes, have agents search around for water sources, once one has found one, it will send the pack information on it's wherabouts and the pack will use the nodes to traverse the fastest (and safest) path to the waterhole
	// If an agent encounters an enemy pack while searching for watering holes it will flee and make the cost of the nearest nodes rise, noting that predators are in that area
	

	// FINISHED CONTENT

	// To enable flexibility with other agent groups, change the reference holders to hold the unordered_map of m_Agents, and search through agents vectors by accessing through their strings  DONE
	// Change flocking behaviour to act around their leader, (have a check if the agent that is being checked is the leader, if so alter the resulting force appropriately, either hardcode or have a blackboard "LeadersWeight")  DONE
	// Set up all agents with a keyboard force which doesn't become activated unless that agent has been specified as being player controlled // Also requires for the other forces to become disabled  DONE
	// Make new behaviour to check if flock target is still within range, sets flocks bool if true, so the final flock member will know whether the flock can still see the target or not  DONE
	// Find out reason for predator slow speed during chase  DONE
	// Update prey to match predator in functionality  DONE
	// Work out issue with dijkstras search	 DONE

	///////////////////////////////////////////////////////////////////////////////////

	// MEMORY LEAKS

	// Prey Behaviour Tree  FIXED
	// Shared Texture Pointers  FIXED
	// SpriteObject / Sceneobject  FIXED


	unsigned int PreySpawnNum = 20;
	unsigned int PredatorSpawnNum = 40;
	unsigned int TreeSpawnNum = 15;
	unsigned int WaterholeSpawnNum = 5;


	std::forward_list<unsigned int> FlockSpawnNumbers = { // Link to variables if the menu will provide customisation options, alter bootstrap startup to only use startup when loading the sim
	5,  // Prey
	5   // Predators
	};

	// If neccessary add "threat level" bool to container, only if seperate groups exist which would influence our agents to run or seek in the same list
	// Optimisation would include coupling a different container with enumerator values so lookup times for groups would be faster with many groups inside the container
	m_PredatorWatchList = {
		"Prey"
	};

	m_PreyWatchList = {
		"Predators"
	};

	Vector2 PredatorSpawnLoc = { ((float)getWindowWidth() / 4), ((float)getWindowHeight() / 2) };
	Vector2 PreySpawnLoc = { ((float)getWindowWidth() / 1.5f), ((float)getWindowHeight() / 2) };
	Vector3 PredatorColour{ 1, 0, 0 };
	Vector3 PreyColour { 0, 1, 0 };

	Sphere PreySpawnZone(PreySpawnLoc, 100);
	Sphere PredatorSpawnZone(PredatorSpawnLoc, 100);

	vector<Sphere*> SpawnZones = {
		&PredatorSpawnZone,
		&PreySpawnZone
	};


	vector<Sphere*> Colliders;
	Colliders.reserve(TreeSpawnNum + WaterholeSpawnNum + SpawnZones.size());

	Colliders.push_back(&PredatorSpawnZone);
	Colliders.push_back(&PreySpawnZone);

	for (unsigned int i = 0; i < WaterholeSpawnNum; i++) {
		m_SceneObjects.push_back(new SpriteObject(Waterhole_tex, 140, 140, Vector2(), 70.f, true));
		Colliders.push_back(m_SceneObjects.at(i)->GetCollider());
	}

	for (unsigned int i = 0; i < TreeSpawnNum; i++) {
		m_SceneObjects.push_back(new SpriteObject(Tree_tex, 240, 240, Vector2(), 100.f, true));
		Colliders.push_back(m_SceneObjects.at(i)->GetCollider());
	}

	// Find random spawn locations for sceneobjects
	for (auto& SO : m_SceneObjects)
	{
		SO->SetPosition(FindRandomSSSpawnLocation(*SO->GetCollider(), Colliders));
	}


	std::forward_list<unsigned int>::iterator it = FlockSpawnNumbers.begin();

	// Fill Flocks
	for (auto& f : m_AgentFlocks)
	{
		for (size_t i = 0; i < *it; ++i)
		{
			std::stringstream s;
			s << "Flock " << (i + 1);

			Flock flock(s.str());
			f.second.push_back(flock);
		}

		it++; // Increment flock number iterator
	}

	// Prey Spawn
	for (auto& flock : m_PreyFlocks)
	{
		for (size_t i = 0; i < (PreySpawnNum / m_PreyFlocks.size()); ++i)
		{
			std::stringstream s;
			s << "Prey " << (i + 1);
			
			Agent* prey = new Prey(flock, s.str(), FindSpawnLocation(PreySpawnZone), PreyColour, m_Agents, m_PreyWatchList, this, m_SceneObjects);
			m_Prey.push_back(prey);
		}
	}

	// Predator Spawn
	for (auto& flock : m_PredatorFlocks)
	{
		for (size_t i = 0; i < (PredatorSpawnNum / m_PredatorFlocks.size()); ++i)
		{
			std::stringstream s;
			s << "Predator " << (i + 1);

			Agent* predator = new Predator(flock, s.str(), FindSpawnLocation(PredatorSpawnZone), PredatorColour, m_Agents, m_PredatorWatchList, this, m_SceneObjects, m_NodeGraph);
			m_Predators.push_back(predator);
		}
	}

	m_NodeGraph.AddApplication(this);
	m_NodeGraph.GenerateNodeTree(m_SceneObjects);


	//Spawn Player
	m_Player = new Prey(m_PreyFlocks.at(0), "Player", { 700, 200 }, { 0, 1, 0 }, m_Agents, m_PreyWatchList, this, m_SceneObjects);

	m_Player->AddBehaviour(new KeyboardBehaviour());
	m_Player->AddBehaviour(new WrapScreenBehaviour(this));
	m_Player->AddBehaviour(new DragBehaviour());
	m_Player->AddBehaviour(new DebugBehaviour());

	// Setup data on the agent's blackboard that's used by (& shared between) its Behaviours
	m_Player->GetBlackboardData("KeyboardAcceleration").FloatData = 400;
	m_Player->GetBlackboardData("Drag").FloatData = 0.f;
	m_Player->SetState(PlayerControlled);

	m_Prey.push_back(m_Player);
	
	m_Agents = {
	{"Prey", m_Prey },
	{"Predators", m_Predators }
	};

	// Set all agents to display their debug info
	for (auto& a : m_Agents)
	{
		for (auto& agent : a.second) 
				agent->GetBlackboardData("DebugOn").BoolData = true;		
	}

	m_timer = 0;

	return true;
}

void Application2D::shutdown() {

	for (auto& a : m_Agents)
	{
		for (auto& agent : a.second)
				delete agent;
	}

	for (auto& SO : m_SceneObjects)
	{
		delete SO;
	}
	
	delete m_font;
	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {

	m_timer += deltaTime;

	// input example
	aie::Input* input = aie::Input::getInstance();

	// Example of how we can modify an Agent's blackboard data at runtime, and the relevant Behaviour will change 
	for (int key = aie::INPUT_KEY_0; key <= aie::INPUT_KEY_9; key++)
	{
		if (input->wasKeyPressed(key))
		{
			m_Player->GetBlackboardData("Drag").FloatData = (float)(key - aie::INPUT_KEY_0) / 10;
		}
	}
	
	//  Flip all debugs
	if (input->wasKeyPressed(aie::INPUT_KEY_B))
	{
		for (auto& a : m_Agents)
		{
			for (auto& agent : a.second)
				agent->GetBlackboardData("DebugOn").BoolData = !agent->GetBlackboardData("DebugOn").BoolData;		
		}

		for (auto& SO : m_SceneObjects)
		{
			SpriteObject* Sprite_obj = dynamic_cast<SpriteObject*>(SO);
			if (Sprite_obj != nullptr)
			{
				Sprite_obj->m_DrawCollision = !Sprite_obj->m_DrawCollision;
			}
		}
	}

	// Flip to make predator leaders controllable // CURRENTLY BORKEN FROM CHANGES
	if (input->wasKeyPressed(aie::INPUT_KEY_K))
	{
		for (auto& agent : m_PredatorFlocks)
		{
			Agent*& leader = agent.GetFlockLeader();

			if (leader->GetState() != PlayerControlled) {
				leader->ChangeState(PlayerControlled);
			}			
			else {
				leader->ResetState();
			}
		}
	}
	
	for (auto& SO : m_SceneObjects)
	{
		SO->Update(deltaTime);
	}

	for (auto& a : m_Agents)
	{
		for (auto& agent : a.second)	
			agent->Update(deltaTime);	
	}

	// Update the camera position using the arrow keys
	float camPosX;
	float camPosY;
	m_2dRenderer->getCameraPos(camPosX, camPosY);

	if (input->isKeyDown(aie::INPUT_KEY_W))
		camPosY += 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_S))
		camPosY -= 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_A))
		camPosX -= 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_D))
		camPosX += 500.0f * deltaTime;

	m_2dRenderer->setCameraPos(camPosX, camPosY);

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application2D::draw() {

	clearScreen();

	m_2dRenderer->begin();

	for (auto& a : m_Agents)
	{
		for (auto& agent : a.second) 	
			agent->Draw(m_2dRenderer);	
	}

	m_NodeGraph.Draw(m_2dRenderer);
	
	for (auto& SO : m_SceneObjects)
	{
		SO->Draw(m_2dRenderer);
	}

	// output some text, uses the last used colour
	char fps[32];
	sprintf_s(fps, 32, "FPS: %i", getFPS());
	m_2dRenderer->setRenderColour(1, 1, 1);
	m_2dRenderer->drawText(m_font, fps, 0, (float)getWindowHeight() - 32);
	m_2dRenderer->drawText(m_font, "Press ESC to quit!", 0, (float)getWindowHeight() - 64);

	std::stringstream s;
	s << "Timer: " << m_timer;
	m_2dRenderer->drawText(m_font, s.str().c_str(), 0, (float)getWindowHeight() - 96);

	// done drawing sprites
	m_2dRenderer->end();
}


Vector2 Application2D::FindSpawnLocation(const Sphere & spawnZone)
{
	Vector2 desiredLocation;
	std::uniform_real<float> distributionAngle(0, 6.28f);
	std::uniform_real<float> distributionLength(0.1f, spawnZone.radius);

	float angle = distributionAngle(generator);
	float length = distributionLength(generator);
	desiredLocation = spawnZone.center;

	desiredLocation.x += (std::sin(angle) * length); 
	desiredLocation.y += (std::cos(angle) * length);

	return desiredLocation;
}


Vector2 Application2D::FindRandomSSSpawnLocation(Sphere& thisCollider, const vector<Sphere*>& avoidColliders)
{
	bool overlapDetected;
	Vector2 desiredLocation;
	int check = 0; // number of iterations till proper spot was found

	do {
		check++;
		overlapDetected = false;
		std::uniform_int<int> distributionX(100, getWindowWidth() - 100);
		std::uniform_int<int> distributionY(100, getWindowHeight() - 100);

		desiredLocation.x = (float)distributionX(generator);
		desiredLocation.y = (float)distributionY(generator);

		thisCollider.center = desiredLocation;

		for (auto& collider : avoidColliders)
		{
			if (collider == &thisCollider)
				continue;

			// If overlap was detected break and try again
			if (collider->overlaps(thisCollider)) {
				overlapDetected = true;
				break;
			}
		}

		std::cout << check << ", ";

	} while (overlapDetected);

	return desiredLocation;
}
