#pragma once
#include "FSM_State.h"

class ChaseState : public State
{
public:
	ChaseState(Agent* target);
	~ChaseState();

	virtual void Update(Agent* agent, float deltaTime) override;
	virtual void Init(Agent* agent) override;
	virtual void Exit(Agent* agent) override;

protected:

	Agent* m_Target;
};

