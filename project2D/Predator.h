#pragma once
#include "Animal.h"
#include "Pathfinding.h"

namespace aie {
	class Application;
}
class SceneObject;

using namespace Pathfinding;

class Predator :
	public Animal
{
public:

	typedef std::unordered_map<std::string, BlackboardData> BlackBoard_Map;
	typedef std::forward_list <std::string> Agent_Watch_List;

	Predator(Flock& sharedFlock, const std::string& name, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph);
	Predator(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector3& col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph);
	Predator(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector2& vel, const Vector3& col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph);
	virtual ~Predator();

	float	GetAttack() { return m_AttackDamage; }

private:

	const void Init(Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph);
	aie::Application* m_App;
	float m_AttackDamage = 10;
};