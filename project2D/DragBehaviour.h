#pragma once
#include "Behaviour.h"

class DragBehaviour : public Behaviour
{
public:
	DragBehaviour();
	virtual ~DragBehaviour();

	virtual bool Update(Agent* agent, float deltaTime);
};

