#pragma once
#include "Behaviour.h"
class FollowLeaderBehaviour :
	public Behaviour
{
public:
	FollowLeaderBehaviour();
	virtual ~FollowLeaderBehaviour();

	virtual bool Update(Agent* agent, float deltaTime);
};

