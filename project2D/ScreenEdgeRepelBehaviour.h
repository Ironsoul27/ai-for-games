#pragma once
#include "Behaviour.h"

namespace aie {
	class Application;
}

class ScreenEdgeRepelBehaviour : public Behaviour
{
public:
	ScreenEdgeRepelBehaviour(aie::Application* app) { m_App = app; }
	virtual ~ScreenEdgeRepelBehaviour() {}

	virtual bool Update(Agent* agent, float deltaTime);

protected:
	aie::Application* m_App;
};

