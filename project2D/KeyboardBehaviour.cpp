#include "KeyboardBehaviour.h"
#include "Input.h"
#include "Agent.h"



KeyboardBehaviour::KeyboardBehaviour()
{
}


KeyboardBehaviour::~KeyboardBehaviour()
{
}


bool KeyboardBehaviour::Update(Agent * agent, float deltaTime)
{
	// If agent is not player controlled
	if (agent->GetState() != PlayerControlled || (agent->BlackboardDataExists("IsDead") && agent->GetBlackboardData("IsDead").BoolData))
		return true;

	aie::Input* input = aie::Input::getInstance();

	float acceleration = 300;

	// Override the default if data has been set on the Agent's Blackboard
	if (agent->BlackboardDataExists("KeyboardAcceleration"))
		acceleration = agent->GetBlackboardData("KeyboardAcceleration").FloatData;

	Vector2 accel = { 0, 0 };

	if (input->isKeyDown(aie::INPUT_KEY_UP))		accel.y = acceleration;
	if (input->isKeyDown(aie::INPUT_KEY_DOWN))		accel.y = -acceleration;
	if (input->isKeyDown(aie::INPUT_KEY_LEFT))		accel.x = -acceleration;
	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))		accel.x = acceleration;

	//const Vector2& vel = agent->GetVelocity();
	agent->SetVelocity(agent->GetVelocity() + (accel * deltaTime));

	return true;
}
