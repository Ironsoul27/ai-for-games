#pragma once
#include "Vector2.h"
#include "Application.h"
#include <vector>
#include <list>
#include <string>

using std::vector;
using std::list;
using std::string;

namespace aie
{
	class Renderer2D;
	class Application;
}

	class Sphere;
	class SceneObject;

namespace Pathfinding {

	struct Node;

	struct Edge {
		Node* target;
		float cost;
	};

	// Encapsulate the data required for a single Node in the Graph
	struct Node {

		Node();
		Node(const string& id, const Vector2& pos);
		virtual ~Node() {}

		void	AddConnection(Node& other, float cost = 1, bool addZeroCostConnections = true)
		{
			if (addZeroCostConnections || (cost > 0))
				connections.push_back({ &other, cost });
		}

		string id = "";
		Vector2 position;

		// Used in dijkstras search and A*
		float gScore;
		float hScore;
		float fScore;
		Node* parent;
		bool  hasBeenVisited = false;
		bool  isNavigatable = true;

		vector<Edge> connections;
	};

	struct SubSection {
		Vector2 position;
		//Node* node = nullptr;
	};


	class NodeGraph
	{
	public:
		void			AddNode(const Node& node);

		bool			ConnectNodes(const string& nodeId1, const string& nodeId2, bool isTwoWay = true, float cost = 1);

		Node*			FindClosestNode(const Vector2& pos);

		Node&			GetNode(unsigned int index);

		Node*			FindNodeById(const string& id);

		void			Draw(aie::Renderer2D* renderer);

		size_t const	GetGraphSize();

		// Perform a pathdfinding search between start/end nodes and return the
		// list of nodes that give the shortest path from start->end
		std::list<Node*> DijkstraSearch(Node* startNode, Node* endNode);
		std::list<Node*> AStarSearch(Node* startNode, Node* endNode);

	protected:
		vector<Node>	m_Nodes;
	};


	class NodeGraph2D
		: public NodeGraph
	{
	public:
		NodeGraph2D() {}
		NodeGraph2D(aie::Application* app) { m_App = app; }
		virtual ~NodeGraph2D() {}

		void			GenerateNodeTree(const vector<SceneObject*>& sceneObjects);
		Node&			GetNode2D(unsigned int x, unsigned int y);
		unsigned int	GetNodeLocationInContainer(unsigned int x, unsigned int y);
		void			AddApplication(aie::Application* app) { m_App = app; }
		bool			AttemptToConnectNodes(const Node& node1, const Node& node2, const vector<SceneObject*>& sceneObjects);
		bool			IsConnectedToNode(const Node& node1, const Node& node2);
		
	protected:
		unsigned int m_Width;
		unsigned int m_Height;

		aie::Application* m_App;
	};
}
