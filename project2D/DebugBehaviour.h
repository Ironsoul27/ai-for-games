#pragma once
#include "Behaviour.h"
#include <string>

class DebugBehaviour : public Behaviour
{
public:
	DebugBehaviour();
	~DebugBehaviour();

	virtual bool Update(Agent* agent, float deltaTime) override;
	virtual void Draw(Agent* agent, aie::Renderer2D* renderer) override;

protected:

	void drawText(Agent* agent, aie::Renderer2D* renderer, const std::string& text);
	void drawBlackboardFloat(Agent* agent, aie::Renderer2D* renderer, const std::string& blackboardId);
	void drawBlackboardVector2(Agent* agent, aie::Renderer2D* renderer, const std::string& blackboardId);
	void drawBlackboardRay(Agent* agent, aie::Renderer2D* renderer, const std::string& blackboardId);
	void drawFloat(Agent* agent, aie::Renderer2D* renderer, const std::string& prefix, const float data);
	void drawBool(Agent* agent, aie::Renderer2D* renderer, const std::string& prefix, bool data);

	float m_TextYPos;
};

