#pragma once
#include "AnimalStates.h"

class Agent;

enum BehaviourResult
{
	Success,
	Failure,
	Ongoing
};

class IBehaviour
{
public:
	IBehaviour() {}
	virtual ~IBehaviour() {}

	virtual BehaviourResult execute(Agent* agent, float deltaTime) = 0;
};
