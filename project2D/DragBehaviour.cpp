#include "DragBehaviour.h"
#include "Agent.h"


DragBehaviour::DragBehaviour()
{
}


DragBehaviour::~DragBehaviour()
{
}

bool DragBehaviour::Update(Agent * agent, float deltaTime)
{
	float drag = 0.5f;
	if (agent->BlackboardDataExists("Drag"))
		drag = agent->GetBlackboardData("Drag").FloatData;

	const Vector2& vel = agent->GetVelocity();
	agent->SetVelocity(vel * (1.0f - drag * deltaTime));
	return true;
}
