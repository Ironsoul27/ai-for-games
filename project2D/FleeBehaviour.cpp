#include "FleeBehaviour.h"
#include "Agent.h"


FleeBehaviour::FleeBehaviour()
{
}


FleeBehaviour::~FleeBehaviour()
{
}

bool FleeBehaviour::Update(Agent* agent, float deltaTime)
{
	
		if (m_Target == nullptr || agent == nullptr) {
			return false;

		Vector2 m_startPos = agent->GetPosition();
		Vector2 m_targetPos = m_Target->GetPosition();

		Vector2 facing = m_startPos - m_targetPos;
		facing.normalise();
		//facing.clamp(agent->GetMaxForce());

		facing += facing * 120;
		facing -= agent->GetVelocity();

		return true;
	}

	return false;
}
