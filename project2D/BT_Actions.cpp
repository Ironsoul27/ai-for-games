#include "BT_Actions.h"
#include "Agent.h"
#include "Animal.h"
#include "Flock.h"
#include "Prey.h"
#include "Predator.h"
#include <assert.h>
#include <algorithm>
#include <iostream>

ChaseAction::ChaseAction()
{}

ChaseAction::~ChaseAction() 
{}

BehaviourResult ChaseAction::execute(Agent* agent, float deltaTime)
{
	agent->ChangeState(SeekPrey);

	return Success;
}


BehaviourResult WanderAction::execute(Agent* agent, float deltaTime)
{
	// Required to keep running 
	agent->ChangeState(Wander);

	return Success;
}

BehaviourResult RemoveFlockTarget::execute(Agent * agent, float deltaTime) // Gets triggered even when flock can still see target
{
	agent->GetFlock().ClearFlockBlackboardData("PackTarget");
	return Success;
}


InRangeCheck::InRangeCheck(Agent_UOM& Targets, Agent_Watch_List& agentwatchlist, float range)
	: m_Agents(Targets), m_RangeSqr(range * range), m_AgentWatchList(agentwatchlist)
{
}

BehaviourResult InRangeCheck::execute(Agent * agent, float deltaTime) 
{
	if (agent->BlackboardDataExists("RangeCheck"))
		m_RangeSqr = agent->GetBlackboardData("RangeCheck").FloatData;

	// Iterate over Agents, and check if their key equals whats in the agents watchlist

	Agent_Watch_List::iterator it;

	// Iterate over watchlist and send the string to the container of agents to get the assigned vector of agents 
	for (it = m_AgentWatchList.begin(); it != m_AgentWatchList.end(); ++it)
	{
		vector<Agent*>* v_ptr;
		v_ptr = &m_Agents.at(*it);

		if (v_ptr != nullptr)
		{
			// Iterate over agents in that group
			for (auto& a : *v_ptr)
			{
				// If target is dead move onto next target
			/*	if (a->BlackboardDataExists("IsDead"))
					if (a->GetBlackboardData("IsDead").BoolData)
						continue;*/

				Vector2 dir = a->GetPosition();
				dir -= agent->GetPosition();

				if (dir.magnitudeSqr() < m_RangeSqr)
				{
					// Blackboard Alterations

					// Prey 
					Prey* prey = dynamic_cast<Prey*>(agent);
					if (prey != nullptr)
					{

						// The enemy flock is not already added to the flee list 
						if (!prey->GetFlock().IsFlockInList(a->GetFlockId())) {							
							prey->GetFlock().AddFleeFlock(a->GetFlockId(),  a->GetFlock());
						}
						else if (prey->GetFlock().IsFlockInList(a->GetFlockId()))
						{
							prey->GetFlock().SetCanBeSeen(a->GetFlockId(), true);
						}

						// Prey must check whether

						//prey->BlackboardDataExists("Fleeing") && prey->GetBlackboardData("Fleeing").BoolData  &&

						// If prey is not running from something 

						// If (Prey do have something that they're running from  &&  The tested member is a member of one of the flocks that this prey group is currently fleeing from)
						// Mark that group as still visible
						// Mark bool canbeseen to true by passing through the enemy flocks id to grab the same group
						// Final member needs to iterate over list of enemy flocks and check if they where seen by any group member

						// New condition class for prey, can the flock still see any members of their attacking flocks? If no member of one of the flock is seen, that flock is removed from the fleeflocks container
					}

					// Predators
					else {
						Predator* predator = dynamic_cast<Predator*>(agent);
						if (predator != nullptr)
						{
							// If we don't have a pack target
							if (!agent->GetFlock().FlockBlackboardDataExists("PackTarget"))
								// Set this agent to become the pack target
								agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData = a;

							// If the agent tested is our current pack target
							else if (a == agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData)
								// Set to true that we can still see the target
								agent->GetFlock().GetFlockBlackboardData("CanStillSeeTarget").BoolData = true;
						}
					}

					return Success; // Required in order to run the rest of the branch
				}
			}
		}
	}

	return Success;
}

BehaviourResult ClearFlockTarget::execute(Agent * agent, float deltaTime)
{
	if (agent->GetFlock().FlockBlackboardDataExists("PackTarget")) {
		agent->GetFlock().ClearFlockBlackboardData("PackTarget");

		vector<Agent*>* Agents = &agent->GetFlock().GetFlockAgents();
		for (auto& a : *Agents)
		{
			if (a->BlackboardDataExists("PackTarget")) {
				a->GetBlackboardData("PackTarget").AgentData = nullptr;
				//std::cout << "Pack Target Reset\n";
			}
			//a->ResetState();
			a->SetState(FollowLeader);
		}
		
		agent->GetFlock().GetFlockLeader()->SetState(Wander);

		return Success;
	}
	return Failure;
}


BehaviourResult AttackTarget::execute(Agent * agent, float deltaTime)
{
	float attackSpeed = .5f;

	if (agent->BlackboardDataExists("AttackSpeed"))
		attackSpeed = agent->GetBlackboardData("AttackSpeed").FloatData;

	m_AttackTimer += deltaTime;

	if (m_AttackTimer >= attackSpeed)
	{
		Predator* predator = dynamic_cast<Predator*>(agent);
		if (predator != nullptr)
		{
			Agent*& target = agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData;

			Prey* prey = dynamic_cast<Prey*>(target);
			if (prey != nullptr)
			{
				prey->TakeDamage(predator->GetAttack());
			}		
		}

		m_AttackTimer = 0;
	}

	return Success;
}


BehaviourResult MoveToEatTarget::execute(Agent * agent, float deltaTime)
{
	if (agent->GetFlock().FlockBlackboardDataExists("MoveToTarget") && agent->GetFlock().GetFlockBlackboardData("MoveToTarget").BoolData)
		return Success;

	vector<Agent*>* m_Agents = &agent->GetFlock().GetFlockAgents();

	if (m_Agents != nullptr)
	{
		for (auto& a : *m_Agents)
		{
			a->GetBlackboardData("SeperationRadius").FloatData = 500;
			a->GetBlackboardData("MaxSeperation").FloatData = 500;
		}
	}
	
	if (agent->GetFlock().FlockBlackboardDataExists("HungriestMember"))
	{
		Agent*& hungriestMember = agent->GetFlock().GetFlockBlackboardData("HungriestMember").AgentData;

		hungriestMember->GetBlackboardData("SeperationRadius").FloatData = 10;
		hungriestMember->GetBlackboardData("MaxSeperation").FloatData = 10;
	}
	
	agent->GetFlock().GetFlockBlackboardData("MoveToTarget").BoolData = true;

	return Success;
}


BehaviourResult FleeAction::execute(Agent * agent, float deltaTime)
{
	agent->ChangeState(Flee);
	return Success;
}


BehaviourResult ClearFleeFlocks::execute(Agent * agent, float deltaTime)
{
	Prey* prey = dynamic_cast<Prey*>(agent);
	if (prey != nullptr)
	{
		Flee_Agents_Map* FleeFlocks = &prey->GetFlock().GetFleeFlocks();
		Flee_Agents_Map::iterator it;

		if (FleeFlocks != nullptr && FleeFlocks->size() != 0)
		{
			if (FleeFlocks != nullptr)
			
			//for (it = FleeFlocks->begin(); it != FleeFlocks->end(); it++)
			//{
			//	if (FleeFlocks->size() == 0 || it._Ptr == nullptr)
			//		continue;
			//	if (it == FleeFlocks->end() && it._Ptr == nullptr)
			//		break;

			//	// If flock members weren't seen this frame
			//	if (it->second.second == false) { // Still returning exception when fleeflocks has a flock in it
			//		prey->GetFlock().RemoveFleeFlock(it->second.first.GetId());
			//	}
			//	else // Reset bool for next frame check
			//		it->second.second = false;

			//	/*if (it == FleeFlocks->end())
			//		break;*/
			//}

			for (auto& flock : *FleeFlocks)
			{
				if (FleeFlocks->size() == 0/* || flock == FleeFlocks->*/)
					break;

				// If flock members weren't seen this frame
				if (flock.second.second == false) { // Still returning exception when fleeflocks has a flock in it
					prey->GetFlock().RemoveFleeFlock(flock.second.first.GetId());
				}
				else // Reset bool for next frame check
					flock.second.second = false;
			}
		}
		std::cout << "FleeFlocks pointer return null\n";
	}
	else
		assert(prey != nullptr);

	return Success;
}

BehaviourResult SetDefaultStates::execute(Agent * agent, float deltaTime)
{
	vector<Agent*>* Agents = &agent->GetFlock().GetFlockAgents();
	for (auto& a : *Agents)
	{
		a->ResetState();
	}

	return Success;
}

BehaviourResult AssignEatTarget::execute(Agent * agent, float deltaTime)
{
	// If already set don't reiterate and set values again
	/*if (agent->GetBlackboardData("HasBeenAssigned_" + Suffix).BoolData)
		return Success;*/

	agent->GetFlock().GetFlockBlackboardData("EatTarget").AgentData = agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData;

	// In updateneccessites must change update pack target if agent doesn't have an eatTarget to not change that agents packtarget when seeking to target to eat, OR make the eattarget state use the eattarget instead of the packtarget

	vector<Agent*>* Agents = &agent->GetFlock().GetFlockAgents();
	
	for (auto& a : *Agents) {
		a->ChangeState(EatTarget);
	}

	if (agent == agent->GetBlackboardData("HungriestMember").AgentData)
		agent->SetState(SeekPrey);

	return Success;
}


BehaviourResult ConsumeTarget::execute(Agent * agent, float deltaTime)
{
	Agent*& target = agent->GetBlackboardData("EatTarget").AgentData;
	//delete target;

	if (agent->GetFlock().FlockBlackboardDataExists("PackTarget"))
		agent->GetFlock().ClearFlockBlackboardData("PackTarget");

	Animal* animal = dynamic_cast<Animal*>(agent);
	if (animal != nullptr)
	{
		animal->SetHunger(100);
	}
	else
		assert(animal != nullptr);

	vector<Agent*>* Agents = &agent->GetFlock().GetFlockAgents();
	for (auto& a : *Agents)
	{
		//a->ClearBlackboardData("PackTarget");
		// May need to check if this is the hungriest member
		a->ClearBlackboardData("EatTarget");
		a->SetState(FollowLeader);
	}

	//agent->GetFlock().ClearFlockBlackboardData("HungriestMember");

	agent->GetFlock().GetFlockLeader()->SetState(Wander);

	return Success;
}
