#pragma once
#include "Behaviour.h"

namespace aie {
	class Application;
}

class WrapScreenBehaviour :
	public Behaviour
{
public:
	WrapScreenBehaviour(aie::Application* app);
	virtual ~WrapScreenBehaviour();

	virtual bool Update(Agent* agent, float deltaTime);

protected:
	aie::Application* m_App;
};

