#pragma once
#include "Agent.h"
#include "AnimalStates.h"
#include "Vector2.h"
#include <vector>
#include <list>

namespace aie
{
	class Application;
}

class SceneObject;
class Ray;
class Sphere;
class SceneObject;

class SteeringForce
{
public:
	SteeringForce();
	virtual ~SteeringForce();

public:
	// derived classes implement to provide implementation
	virtual Vector2 CalculateForce(Agent* agent, float deltaTime) = 0;

};


//class FleeForce : public SteeringForce {
//
//public:
//
//	//SeekForce(Agent* target, float dir, float speed);
//	FleeForce(/*const std::string& blackboardPrefix = "Seek"*/) {}
//
//	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);
//
//protected:
//
//	//std::string m_BlackboardPrefix;
//};




/////////////
// Wander Force
// Two variables to control behaviour
// Wander radius controls how big circle is
// Wander Distance controls how far in front of agent the circle is

class WanderForce : public SteeringForce {
public:
	WanderForce(aie::Application* app, vector<SceneObject*>& sceneObjects);

	void		RecalculateTarget();

	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);

protected:
	void		updateWanderTarget(Agent* agent);

	// Return a random value between -0.5 and 0.5
	float		getRandomValue();

protected:

	aie::Application* m_App;
	vector<SceneObject*>& m_SceneObjects;
	bool m_CalculateTargetRequired = true;

};



//class SeperationForce : public SteeringForce {
//public:
//	SeperationForce(vector<Agent*>& agents, std::list<Agent*>& neighbourhoodList);
//
//	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);
//
//private:
//	std::list<Agent*>& m_Neighbourhood;
//	vector<Agent*>& m_Flock;
//};


//class AlignmentForce : public SteeringForce {
//public:
//	AlignmentForce(vector<Agent*>& agents, std::list<Agent*>& neighbourhoodList);
//
//	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);
//
//private:
//	std::list<Agent*>& m_Neighbourhood;
//	vector<Agent*>& m_Flock;
//};
//
//
//
//
//class FlockForces : public SteeringForce {
//public:
//	FlockForces(vector<Agent*>& agents);
//	virtual ~FlockForces();
//	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);
//
//protected:
//	void calculateNeighbourhood(Agent* agent);
//
//	std::list<Agent*> m_Neighbourhood;
//	vector<Agent*>& m_Flock;
//
//	//SeperationForce* m_SeperationForce = new SeperationForce(m_Flock, m_Neighbourhood);
//	//AlignmentForce* m_AlignmentForce = new AlignmentForce(m_Flock, m_Neighbourhood);
//	//CohesionForce* m_CohesionForce = new CohesionForce(m_Flock, m_Neighbourhood);
//
//	std::list<SteeringForce*> m_FlockForces = {
//		//m_SeperationForce,
//		//m_AlignmentForce,
//		//m_CohesionForce
//	};
//};

class CohesionForce : public SteeringForce {
public:
	CohesionForce() {}

	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);
};

class SeekFleeForce : public SteeringForce
{
public:
	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);
};


class SeperationForce : public SteeringForce
{
public:
	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);

};

class CollisionAvoidance
	: public SteeringForce
{
public:

	CollisionAvoidance(std::vector<SceneObject*>& sceneObjectGroup);
	virtual ~CollisionAvoidance() {}

	virtual Vector2 CalculateForce(Agent* agent, float deltaTime);

	void const addSphereColliders(std::vector<SceneObject*>& sphereGroup) { m_sceneObjects = sphereGroup; }

protected:
	Sphere* FindMostThreatining(Agent* agent, Ray& ahead);
	std::vector<SceneObject*>& m_sceneObjects;
};
