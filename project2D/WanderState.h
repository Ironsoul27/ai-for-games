#pragma once
#include "FSM_State.h"

class WanderState : public State
{
public:
	WanderState();
	~WanderState();

	virtual void Update(Agent* agent, float deltaTime) override;
	virtual void Init(Agent* agent) override;
	virtual void Exit(Agent* agent) override;
};

