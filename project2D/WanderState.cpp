#include "WanderState.h"
#include <iostream>
#include <Renderer2D.h>
#include "Agent.h"


WanderState::WanderState()
	: State("Wander")
{
}


WanderState::~WanderState()
{
}

void WanderState::Update(Agent * agent, float deltaTime)
{
	// Don't need to do anything in the update, because the Attached Wander Steering Behaviour does all the work.
}

void WanderState::Init(Agent * agent)
{
	std::cout << "[WanderState::Init]\n";

	agent->GetBlackboardData("WanderOff").BoolData = false;
	agent->GetBlackboardData("WanderRadius").FloatData = 200;
	agent->GetBlackboardData("WanderDistance").FloatData = 0;
	agent->GetBlackboardData("WanderSpeed").FloatData = 20;
}

void WanderState::Exit(Agent * agent)
{
	std::cout << "[WanderState::Exit]\n";

	agent->GetBlackboardData("WanderOff").BoolData = true;
}
