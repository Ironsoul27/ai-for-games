#pragma once
#include "Vector2.h"
#include "Vector3.h"
#include "AnimalStates.h"
#include "SceneObject.h"
//#include "SpriteObject.h"
#include "Ray.h"
#include <unordered_map>
#include <vector>
#include <memory>
#include <utility>
#include <string>


using std::string;
using std::vector;

class Behaviour;
class Flock;
class Agent;

typedef std::unordered_map<string, vector<Agent*>> Agent_UOM;
extern aie::Font* g_SystemFont;

namespace aie
{
	class Renderer2D;
	class Font;
}

class Agent/* : public SpriteObject*/
{
public:

	struct BlackboardData
	{
		BlackboardData()
		{}

		~BlackboardData()
		{}

		union
		{
			Agent*	AgentData;
			int		IntData;
			float	FloatData;
			Vector2 Vector2Data;
			bool	BoolData;
			Ray		RayData;
			SceneObject* SceneObjectData;
		};
	};

public:

	typedef std::unordered_map<std::string, BlackboardData> BlackBoard_Map;

	//std::unordered_map<string, BlackboardData>& sharedBlackboard

	Agent(Flock& sharedFlock, const std::string& name = ""); 
	Agent(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector3& col);
	Agent(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector2& vel, const Vector3& col);

	virtual ~Agent();

	virtual void Update(float deltaTime);

	virtual void Draw(aie::Renderer2D* renderer);

	void AddBehaviour(Behaviour* behaviour);

	// Name functions
	const std::string& GetName() { return m_Name; }
	void SetName(const std::string& name) { m_Name = name; }

	// Movement functions
	void		    SetPosition(Vector2 position) { m_Position = position; }
	const Vector2&  GetPosition() const { return m_Position; }
	void		    SetVelocity(Vector2 velocity) { m_Velocity = velocity; }
	const Vector2&  GetVelocity() const { return m_Velocity; }

	// Colour functions
	void const		SetColour(const Vector3& col) { m_Colour = col; }
	const Vector3&  GetColour() { return m_Colour; }

	// Mass functions
	void			SetMass(float m);
	float			GetMass() const { return m_Mass; }

	const Vector2&	GetMaxForce() { return m_MaxForce; }

	// Access to the blackboard to add / update / remove key/value data
	std::unordered_map<string, BlackboardData>	GetBlackBoard() { return m_Blackboard; }
	bool				BlackboardDataExists(const std::string& id) const { return m_Blackboard.count(id); }
	BlackboardData&		GetBlackboardData(const std::string id) { return m_Blackboard[id]; }
	void				ClearBlackboardData(const std::string& id) { m_Blackboard.erase(id); }

	// Flock functions
	Flock&				GetFlock() { return m_Flock; }
	std::string&		GetFlockId();

	// State functions
	void const			ChangeState(States state)
	{
		/*if (m_PreviousState != PlayerControlled) {*/ // if previous is not player control
			m_PreviousState = m_StateData; // Set previous to current and new state to current
			m_StateData = state;
		//}
		/*else if (state == PlayerControlled)
		{
			m_StateData
		}*/
		/*else
			m_StateData = m_PreviousState;*/
	}
	States				GetState() { return m_StateData; }
	void const			SetState(States state) { m_StateData = state; }
	States				GetPreviousState() { return m_PreviousState; }
	void const			SetPreviousState(States state) { m_PreviousState = state; }
	void const			ResetState() { m_StateData = m_PreviousState; }


protected:

	const void Init();

	std::vector<Behaviour*> m_BehaviourList;
	std::string				m_Name = "";
	Vector2					m_Position;
	Vector2					m_MaxForce = { 1000, 1000 };
	Vector2					m_Velocity;
	Vector3					m_Colour = { 1, 1, 1 };
	float					m_Mass = 1;
	States					m_StateData;
	States					m_PreviousState;

	std::unordered_map<string, BlackboardData> m_Blackboard; 
	Flock& m_Flock;
};