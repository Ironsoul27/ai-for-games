 #pragma once
#include "IBehaviour.h"
#include <vector>
#include <forward_list>
#include <string>
#include <unordered_map>

using std::vector;
using std::string;


class IsFlockMemberHungry :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class IsHungry :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class IsThirsty :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class IsLastMember:
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class CanFlockNotSeeTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class DoesFlockHaveTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class CloseEnoughToAttack :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class IsTargetDead :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class IsHungriestMember :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class CloseEnoughToEat:
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class AreFleeFlocksRemaining :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};



//class CloseEnoughToEat :
//	public IBehaviour
//{
//public:
//	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
//};