#include "SeekBehaviour.h"
#include "Agent.h"



SeekBehaviour::SeekBehaviour()
{
}


SeekBehaviour::~SeekBehaviour()
{
}

bool SeekBehaviour::Update(Agent * agent, float deltaTime)
{
	if (agent == nullptr) {
		return Vector2(0, 0);
	}
	Vector2 m_startPos = agent->GetPosition();
	Vector2 m_targetPos = m_target->GetPosition();

	Vector2 facing = m_targetPos - m_startPos;
	facing.normalise();
	//facing.clamp(agent->GetMaxForce());

	facing += facing * 120;
	facing -= agent->GetVelocity();

	return true;

	//return facing;
}

//Vector2 facing = m_target->GetPosition() - agent->GetPosition();

