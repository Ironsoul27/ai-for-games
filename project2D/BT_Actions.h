#pragma once
#include "IBehaviour.h"
#include <forward_list>
#include <unordered_map>
#include <string>
#include <vector>
#include <utility>

using std::string;
using std::vector;

class Agent;

typedef std::unordered_map<string, vector<Agent*>> Agent_UOM;
typedef std::forward_list <std::string> Agent_Watch_List;
//typedef std::pair<Flock&, bool> Flock_Pair;


class ChaseAction :
	public IBehaviour
{
public:
	ChaseAction();
	virtual ~ChaseAction();

	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;

private:
	Agent* m_Target;
};


class WanderAction :
	public IBehaviour
{
public:
	WanderAction() {}
	virtual ~WanderAction() {}

	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};

class RemoveFlockTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class InRangeCheck :
	public IBehaviour
{
public:
	InRangeCheck(Agent_UOM& Targets, Agent_Watch_List& agentwatchlist, float range);
	virtual ~InRangeCheck() {}

	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;

private:
	Agent_UOM& m_Agents;
	Agent_Watch_List& m_AgentWatchList;
	float  m_RangeSqr;
};


class ClearFlockTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class AttackTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;

protected:

	float m_AttackTimer = 0;
};


class MoveToEatTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class FleeAction :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class ClearFleeFlocks :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class SetDefaultStates :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};


class AssignEatTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;

protected:
	//std::string Suffix = "AssignEatTarget";
};


class ConsumeTarget :
	public IBehaviour
{
public:
	virtual BehaviourResult execute(Agent* agent, float deltaTime) override;
};
