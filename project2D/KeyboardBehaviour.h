#pragma once
#include "Behaviour.h"

class KeyboardBehaviour : public Behaviour
{
public:
	KeyboardBehaviour();
	virtual ~KeyboardBehaviour();

	// vector2
	virtual bool Update(Agent* agent, float deltaTime);
};

