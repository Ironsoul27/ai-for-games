#include "FollowNodeTreeBehaviour.h"
#include "Agent.h"
#include <list>

FollowNodeTreeBehaviour::FollowNodeTreeBehaviour(NodeGraph2D& nodegraph, float range)
	: m_NodeGraph(nodegraph), m_CloseEnoughRange(range)
{
}


FollowNodeTreeBehaviour::~FollowNodeTreeBehaviour()
{
}

bool FollowNodeTreeBehaviour::Update(Agent * agent, float deltaTime)
{
	switch (agent->GetState()) {
		case SeekTarget: { // Seek to specific node

			if (m_CurrentNode != nullptr && m_CurrentNode == *m_Path.end())
				return true;

			if (m_UpdateTargetRequired)
			{
				m_CurrentNode = m_NodeGraph.FindClosestNode(agent->GetPosition());
				m_Path = m_NodeGraph.DijkstraSearch(m_CurrentNode, m_TargetNode);
			}
		}

		case NodeTreeTarget: { // Look for target while traversing the graph

			// New Track
			if (m_UpdateTargetRequired) 
			{
				// Assign current node before working out path to ensure current node doesn't equal nullptr
				m_CurrentNode = m_NodeGraph.FindClosestNode(agent->GetPosition());
				m_NextNode = m_CurrentNode;
				GetNewWanderTrack();				
			}

			// Search for target while traversing tree
			if (agent->BlackboardDataExists("FindTarget"))
				m_SeekObject = agent->GetBlackboardData("FindTarget").SceneObjectData;

			if (m_SeekObject != nullptr)
			{
				const Vector2& targetPos = m_SeekObject->GetPosition();
				const Vector2& agentPos = agent->GetPosition();

				const Vector2& dir = targetPos - agentPos;

				if (dir.magnitudeSqr() < (m_CloseEnoughToTarget * m_CloseEnoughToTarget)) {
					// Found the target
				}
			}			
		}
	}

	// Check if close enough to target node and if so change nextNode to next node in list
	Node*& targetNode = *m_Path_It;
	const Vector2& targetPos = targetNode->position;
	const Vector2& agentPos = agent->GetPosition();

	const Vector2& dir = targetPos - agentPos;

	// If agent is close enough move to next node
	if (dir.magnitudeSqr() < (m_CloseEnoughRange * m_CloseEnoughRange))
	{
		std::list<Node*>::iterator tempIt = m_Path_It;
		tempIt++;

		if (tempIt == m_Path.end()) 
			m_UpdateTargetRequired = true;
		else {
			m_Path_It++;
			m_NextNode = *m_Path_It;
		}
	}

	// change seek target
	if (m_NextNode != nullptr)
		agent->GetBlackboardData("SeekTarget").Vector2Data = m_NextNode->position;

	return true;
}

bool FollowNodeTreeBehaviour::GetNewWanderTrack()
{
	bool foundTarget = false;
	unsigned int index;

	while (!foundTarget) {

		// number between 0 and number of nodes in graph
		index = rand() % m_NodeGraph.GetGraphSize();

		Node* tempNode = &m_NodeGraph.GetNode(index);

		// if our test node is not the current node
		if (tempNode != m_CurrentNode) 
			foundTarget = true;
	}


	// assign targetnode to node at index position;
	m_TargetNode = &m_NodeGraph.GetNode(index);

	// create new path between nodes
	m_Path = m_NodeGraph.DijkstraSearch(m_CurrentNode, m_TargetNode);
	m_Path_It = m_Path.begin();

	m_UpdateTargetRequired = false;
	return true;
}
