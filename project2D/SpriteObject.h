#pragma once
#include "SceneObject.h"

typedef std::shared_ptr<aie::Texture> Texture_Shr_Ptr;

class SpriteObject : public SceneObject
{
public:
	SpriteObject();
	SpriteObject(std::string& tagId) { m_TagId = tagId; }
	SpriteObject(Texture_Shr_Ptr& texture) { m_Texture = texture; }
	SpriteObject(Texture_Shr_Ptr& texture, std::string& tagId) { 
		m_Texture = texture;
		m_TagId = tagId;
	}
	SpriteObject(Texture_Shr_Ptr& texture, float texWidth, float texHeight, const Vector2& pos, float collisionRadius, bool drawDebug = false) {
		m_Texture = texture;
		m_TextureWidth = texWidth;
		m_TextureHeight = texHeight;
		m_Position = pos;
		m_Collider.radius = collisionRadius;
		m_DrawCollision = drawDebug;
	}
	virtual ~SpriteObject();

	virtual void	  onDraw(aie::Renderer2D* renderer);

	bool			  SetTexture(Texture_Shr_Ptr& texture) { m_Texture = texture; }
	std::string&	  GetTagId() { return m_TagId; }

	bool			  m_DrawCollision;

protected:
	Texture_Shr_Ptr   m_Texture = nullptr;
	std::string		  m_TagId;

	float			  m_TextureWidth = 200;
	float			  m_TextureHeight = 200;

};

