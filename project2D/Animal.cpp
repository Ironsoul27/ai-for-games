#include "Animal.h"
#include <cmath>



Animal::Animal(Flock& sharedFlock, const std::string name)
	: Agent(sharedFlock, name)
{
	Init();
}

Animal::Animal(Flock & sharedFlock, const std::string & name, const Vector2 & pos, const Vector3 & col)
	: Agent(sharedFlock, name, pos, col)
{
	Init();
}

Animal::Animal(Flock & sharedFlock, const std::string & name, const Vector2 & pos, const Vector2 & vel, const Vector3 & col)
	: Agent(sharedFlock, name, pos, vel, col)
{
	Init();
}


Animal::~Animal()
{
}

void Animal::Init()
{
	GetBlackboardData("Hungry").BoolData = false;
	m_Thirst = RandomFloat(50, 100);
	//m_Hunger = RandomFloat(50, 100);
}

void Animal::SetHealth(const float val)
{
	m_Health = val;
	if (m_Health <= 0) {
		m_Health = 0;
		Death();
	}
}

void Animal::SetHunger(const float val)
{
	m_Hunger = val;
	if (m_Hunger <= 0) {
		m_Hunger = 0;
		Death();
	}
}

void Animal::SetThirst(const float val)
{
	m_Thirst = val;
	if (m_Thirst <= 0) {
		m_Thirst = 0;
		Death();
	}
}

void Animal::SetTiredness(const float val)
{
	m_Tiredness = val;
	if (m_Tiredness <= 0) 
		m_Tiredness = 0;
}

const void Animal::TakeDamage(float damage)
{
	m_Health -= damage;
	if (m_Health <= 0) {
		m_Health = 0;
		Death();
	}	
}

float Animal::RandomFloat(float min, float max)
{
	// Generate random float between 0-1
	float randomFloat = ((float) rand()) / (float) RAND_MAX;
	// Get difference in float paramaters
	float diff = max - min;
	// Times randomfloat by difference in value then add to min value to get a number between (min & max) + min
	float r = randomFloat * diff;
	float x = min + r;
	// Round off
	return (x = round(x));
}

void Animal::Death()
{
	GetBlackboardData("IsDead").BoolData = true;
}
