#pragma once
#include "Behaviour.h"

class UpdateNecessities :
	public Behaviour
{
public:
	UpdateNecessities() {}
	virtual ~UpdateNecessities() {}

	virtual bool Update(Agent* agent, float deltaTime) override;
	
protected:

	float m_Timer = 0;

	void const CalculateHungriestMember(Agent* agent);
};