#include "Pathfinding.h"
#include "Renderer2D.h"
#include "AABB.h"
#include "Ray.h"
#include "Sphere.h"
#include "SceneObject.h"
#include <Renderer2D.h>
#include <sstream>
#include <assert.h>
#include <algorithm>
#include <sstream>
#include <list>


extern aie::Font* g_SystemFont;

namespace Pathfinding {

	// When calculating node points have linear raytrace tests between them to calculate whether there's an object in the way. If so the node point is offset randomly and tried again

	Node::Node()
	{}

	Node::Node(const string & id, const Vector2 & pos)
		: id(id), position(pos)
	{}

	void NodeGraph::AddNode(const Node & node)
	{
		m_Nodes.push_back(node);
	}

	bool NodeGraph::ConnectNodes(const string & nodeId1, const string & nodeId2, bool isTwoWay, float cost)
	{
		Node* node1 = FindNodeById(nodeId1);
		Node* node2 = FindNodeById(nodeId2);

		if (node1 && node2)
		{
			node1->connections.push_back({ node2, cost });

			if (isTwoWay)
				node2->connections.push_back({ node1, cost });

			return true;
		}
		return false;
	}


	Node * NodeGraph::FindClosestNode(const Vector2 & pos)
	{
		Node* returnNode = nullptr;
		float closestNodeDist = FLT_MAX;

		for (auto& node : m_Nodes)
		{
			const Vector2& nodePos = node.position;
			const Vector2& dir = nodePos - pos;

			float distance = dir.magnitudeSqr();
			
			if (distance < closestNodeDist) {
				returnNode = &node;
				closestNodeDist = distance;
			}
		}
		assert(returnNode != nullptr);

		return returnNode;
	}

	Node & NodeGraph::GetNode(unsigned int index)
	{
		if (index >= m_Nodes.size())
			throw "Out Of Bounds";

		return m_Nodes[index];
	}

	Node * NodeGraph::FindNodeById(const string & id)
	{
		for (Node& n : m_Nodes) {
			if (n.id == id) {
				return &n;
			}
		}

		return nullptr;
	}

	void NodeGraph::Draw(aie::Renderer2D* renderer)	
	{
		renderer->setRenderColour(0, 0.5f, 0);

		for (Node& n : m_Nodes) {
			for (Edge& e : n.connections) {

				if (n.isNavigatable) {
					std::stringstream s;
					s << e.cost;

					renderer->drawLine(n.position.x, n.position.y, e.target->position.x, e.target->position.y);
					renderer->drawText(g_SystemFont, s.str().c_str(), (n.position.x + e.target->position.x) / 2, (n.position.y + e.target->position.y) / 2);
				}	
			}
		}


		// Draw the nodes
		//renderer->setRenderColour(0, 0.5f, 0);
		for (Node& n : m_Nodes) 
		{
			renderer->drawCircle(n.position.x, n.position.y, 20);
		}

		// Draw the ID text
		renderer->setRenderColour(1, 1, 1);
		for (Node& n : m_Nodes)
		{
			renderer->drawText(g_SystemFont, n.id.c_str(), n.position.x - 10, n.position.y);
		}
	}

	size_t const NodeGraph::GetGraphSize()
	{
		return m_Nodes.size();
	}

	bool compareNodes(Node* n1, Node* n2)
	{
		return (n1->gScore < n2->gScore);
	}


	std::list<Node*> NodeGraph::DijkstraSearch(Node* startNode, Node* endNode)
	{
		assert((startNode != nullptr) && (endNode != nullptr));

		if ((startNode == nullptr) || (endNode == nullptr))
			throw "Invalid Input";

		std::list<Node*> path; // Alternatively : heap

		if (startNode == endNode)
			return path;

		startNode->gScore = 0;
		startNode->parent = nullptr;

		std::list<Node*> openList;
		std::list<Node*> closedList;
		Node* currentNode;


		openList.push_back(startNode);

		while (!openList.empty())
		{
			openList.sort(compareNodes);

			currentNode = openList.front();

			if (currentNode == endNode)
				break;

			openList.remove(currentNode);
			closedList.push_back(currentNode);

			for (int i = 0; i < currentNode->connections.size(); i++)
			{
				Edge& e = currentNode->connections[i];

				if (std::find(closedList.begin(), closedList.end(), e.target) == closedList.end()) // if node is not in closed list
				//if (!e.target->hasBeenVisited)
				{
					float gScore = currentNode->gScore + e.cost;

					if (std::find(openList.begin(), openList.end(), e.target) == openList.end()) // If node is not in open list
					{
						e.target->gScore = gScore;
						e.target->parent = currentNode;
						e.target->hasBeenVisited = true; /////
						openList.push_back(e.target);
					}
					else if (gScore < e.target->gScore) // Node is already in the openlist with a valid score // So compare the calculated score with the existing to find the shorter path
					{
						e.target->gScore = gScore;
						e.target->parent = currentNode;
						//e.target->hasBeenVisited = true; /////
					}
				}
			}
		}

		currentNode = endNode;

		/*for (auto c : closedList) {
			c->hasBeenVisited = false;
		}*/

		while (currentNode != nullptr)
		{
			path.push_front(currentNode);
			currentNode = currentNode->parent;
		}

		return path;
	}

	float Heuristic(Node* currentNode, Node* endNode)
	{
	/*	const Vector2& pos1 = currentNode->position();
		const Vector2& pos2 = endNode->position();*/
		return 4;
	}

	std::list<Node*> NodeGraph::AStarSearch(Node* startNode, Node* endNode)
	{
		assert((startNode != nullptr) && (endNode != nullptr));

		if ((startNode != nullptr) && (endNode != nullptr))
			throw "Invalid Input";

		std::list<Node*> path; // Alternatively : heap

		if (startNode == endNode)
			return path;

		startNode->gScore = 0;
		startNode->parent = nullptr;

		std::list<Node*> openList;
		std::list<Node*> closedList;
		Node* currentNode;


		openList.push_back(startNode);

		while (!openList.empty())
		{
			openList.sort(compareNodes);

			currentNode = openList.front();

			openList.remove(currentNode);
			closedList.push_back(currentNode);

			for (int i = 0; i < currentNode->connections.size(); i++)
			{
				Edge& e = currentNode->connections[i];

				//if (std::find(closedList.begin(), closedList.end(), e.target) == closedList.end()) // if iterator reached end of list
				if (e.target->hasBeenVisited)
				{
					float gScore = currentNode->gScore + e.cost;
					float hScore = Heuristic(e.target, endNode);
					float fScore = gScore + hScore;

					if (std::find(openList.begin(), openList.end(), e.target) == openList.end())
					{
						e.target->gScore = gScore;
						e.target->fScore = fScore;
						e.target->parent = currentNode;
						openList.push_back(e.target);
					}
					else if (fScore < e.target->fScore)
					{
						e.target->gScore = gScore;
						e.target->fScore = fScore;
						e.target->parent = currentNode;
					}
				}
			}
		}

		currentNode = endNode;

		for (auto c : closedList) {
			c->hasBeenVisited = false;
		}

		while (currentNode != nullptr)
		{
			path.push_front(currentNode);
			currentNode = currentNode->parent;
		}

		return path;
	}



	void NodeGraph2D::GenerateNodeTree(const vector<SceneObject*>& sceneObjects)
	{
		// Calculate different node positions around the map, all seperated by each other by an average distance

		// Distance calculation 
		// 1. Seperate screen into subsections, apply random displacement of node location within subsection bounds, then test if it is within sight of the next node it should connect to (2 or 1 closest nodes to this node, or adjacent nodes from different subsections)
		// If not generate new displacement and try again

		// Implementation Notes
		// Every edge must be 2 way
		// Since calculating edge connections will require knowledge of the node inside adjacent subsections, subsections should be created as a container of AABB, with each holding onto reference of node inside

		// 2. Pick random points within bounds of screenspace (size of window / rand division), if doing this method incorporate a loading screen which would block all draws and updates until node tree was generated (maybe)

		// Need to make decision on whether multiple attempts is worth it, or if single check is enough and if no connection is possible, don't connect the nodes


		const unsigned int verticalSubsections = 7;
		const unsigned int horizontalSubsections = 9;

		m_Height = verticalSubsections;
		m_Width = horizontalSubsections;

		const unsigned int subsectionVerticalBounds = m_App->getWindowHeight() / verticalSubsections;
		const unsigned int subsectionHorizontalBounds = m_App->getWindowWidth() / horizontalSubsections;

		// Generate Subsections

		SubSection subSections[verticalSubsections][horizontalSubsections];

		unsigned int spawnOffsetx = 170;
		unsigned int spawnOffsety = 100;
		unsigned int jitterReduction = 2; // Higher values equals less jitter

		unsigned int reducedHorizontalBounds = subsectionHorizontalBounds / jitterReduction;
		unsigned int reducedVerticalBounds = subsectionVerticalBounds / jitterReduction;

		m_Nodes.clear();
		m_Nodes.reserve(horizontalSubsections * verticalSubsections);

		unsigned int nodeNum = 0;

		// Fill Graph With Nodes
		for (unsigned int y = 0; y < verticalSubsections; y++)
		{
			for (unsigned int x = 0; x < horizontalSubsections; x++)
			{
				nodeNum++;
				SubSection& temp = subSections[y][x];
				Vector2* testPos;

				// Assign centre position of subsection
				temp.position = Vector2((float)spawnOffsetx + x * subsectionHorizontalBounds, (float)spawnOffsety + (verticalSubsections - 1 - y) * subsectionVerticalBounds);

				// Generate random position within subsection bounds
				testPos = &Vector2(temp.position.x + rand() % reducedHorizontalBounds - reducedHorizontalBounds, temp.position.y + rand() % reducedVerticalBounds - reducedVerticalBounds);

				// Create new node at test position
				std::stringstream s;
				s << "Node " << (nodeNum);
				Node node(s.str(), *testPos);

				// If point overlaps with any colliders set as non navigatable
				for (auto& SO : sceneObjects)
				{
					if (SO->GetCollider()->overlaps(*testPos))
					{
						// Set to non navigatable
						node.isNavigatable = false;
					}
				}

				AddNode(node);
			}
		}


		const unsigned int defaultCost = 1;

		for (unsigned int y = 0; y < verticalSubsections; y++)
		{
			for (unsigned int x = 0; x < horizontalSubsections; x++)
			{
				// Give control of node to node pointer
				Node* node = &GetNode2D(x, y);

				// If the neighbouring node is not already connected to the current node  &&  There is an unobstructed line between the nodes
				// Add connection between them
				if (x > 0 && !IsConnectedToNode(*node, GetNode2D(x - 1, y)))
					if (AttemptToConnectNodes(*node, GetNode2D(x - 1, y), sceneObjects))
					{
						GetNode2D(x - 1, y).AddConnection(*node, defaultCost);
					}								
								
				if (x < horizontalSubsections - 1 && !IsConnectedToNode(*node, GetNode2D(x + 1, y)))
					if (AttemptToConnectNodes(*node, GetNode2D(x + 1, y), sceneObjects))
					{
						GetNode2D(x + 1, y).AddConnection(*node, defaultCost);
					}
							
				if (y > 0 && !IsConnectedToNode(*node, GetNode2D(x, y - 1)))
					if (AttemptToConnectNodes(*node, GetNode2D(x, y - 1), sceneObjects))
					{
						GetNode2D(x, y - 1).AddConnection(*node, defaultCost);
					}
								
				if (y < verticalSubsections - 1 && !IsConnectedToNode(*node, GetNode2D(x, y + 1)))
					if (AttemptToConnectNodes(*node, GetNode2D(x, y + 1), sceneObjects))
					{
						GetNode2D(x, y + 1).AddConnection(*node, defaultCost);
					}								

			}
		}
	}

	Node & NodeGraph2D::GetNode2D(unsigned int x, unsigned int y)
	{
		if ((x > m_Width) || (y > m_Height))
			throw "Out Of Bounds";

		return GetNode(y * m_Width + x);
	}

	unsigned int NodeGraph2D::GetNodeLocationInContainer(unsigned int x, unsigned int y)
	{
		if ((x > m_Width) || (y > m_Height))
			throw "Out Of Bounds";

		return (y * m_Width + x);
	}


	bool NodeGraph2D::AttemptToConnectNodes(const Node& node1, const Node& node2, const vector<SceneObject*>& sceneObjects)
	{
		//if (!node1.isNavigatable || !node2.isNavigatable)
		//	return false;

		//Vector2 dir = node1.position - node2.position;
		//Ray intersectRay(node1.position, dir, dir.magnitude()/*node1.position.distance(node2.position)*/);

		//for (auto& SO : sceneObjects)
		//{
		//	if (intersectRay.intersects(*SO->GetCollider()))
		//		return false;
		//}

		// If no interescts were made
		return true;
	}


	bool NodeGraph2D::IsConnectedToNode(const Node& node1, const Node & node2)
	{
		for (auto& C : node2.connections) // If the other node is already connected to the current node
		{
			if (C.target == &node1)
				return true; // Don't connect them
		}
		return false;
	}
	
}