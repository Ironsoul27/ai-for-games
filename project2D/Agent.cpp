#include "Agent.h"
#include "Renderer2D.h"
#include "Behaviour.h"
#include <assert.h>
#include "Flock.h"


Agent::Agent(Flock& sharedFlock, const std::string & name)
	: m_Flock(sharedFlock), m_Name(name)
{
	Init();
}

Agent::Agent(Flock& sharedFlock, const std::string & name, const Vector2 & pos, const Vector3 & col)
	: m_Flock(sharedFlock), m_Name(name), m_Position(pos), m_Colour(col)
{
	Init();
}

Agent::Agent(Flock& sharedFlock, const std::string & name, const Vector2 & pos, const Vector2 & vel, const Vector3 & col)
	: m_Flock(sharedFlock), m_Name(name), m_Position(pos), m_Velocity(vel), m_Colour(col)
{
	Init();
}


Agent::~Agent()
{
	for (auto m_Behaviour : m_BehaviourList) {
		delete m_Behaviour;
	}
}


const void Agent::Init()
{
	m_Flock.Register(this);
}


void Agent::Update(float deltaTime)
{
	for (Behaviour* b : m_BehaviourList)
	{
		if (!b->Update(this, deltaTime))
		{
			// The Behaviour has decided to return false.
			// Log an error if you like....
		}
	}

	// Update the Agent's position // Agents velocity has been updated through the behaviours
	m_Position = m_Position + m_Velocity * deltaTime;
}

void Agent::Draw(aie::Renderer2D* renderer)
{
	assert(renderer != nullptr);

	/*if (BlackboardDataExists("IsDead"))
		if (GetBlackboardData("IsDead").BoolData)
			return;*/

	// Draw the Agent
	renderer->setRenderColour(m_Colour.x, m_Colour.y, m_Colour.z);
	renderer->drawBox(m_Position.x, m_Position.y, 10, 10);

	// Then give the behaviours a chance to draw themselves, if they wish
	for (Behaviour* b : m_BehaviourList)
	{
		b->Draw(this, renderer);
	}
}

void Agent::AddBehaviour(Behaviour* behaviour)
{
	assert(behaviour != nullptr);

	if (behaviour != nullptr)
		m_BehaviourList.push_back(behaviour);
}

void Agent::SetMass(float m)
{
	assert(m > 0);

	if (m > 0) {
		m_Mass = m;
	}
}

std::string & Agent::GetFlockId()
{
	return m_Flock.GetId(); 
}
