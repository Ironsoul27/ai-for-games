#include "FSM_Transition.h"
#include "FSM_Condition.h"
#include <assert.h>

Transition::~Transition()
{
	if (m_Condition != nullptr)
		delete m_Condition;
}

State * Transition::GetTargetState()
{
	 return m_Target;
}

bool Transition::HasTriggered(Agent * agent)
{
	assert(m_Condition != nullptr);

	if (m_Condition != nullptr)
	{
		return m_Condition->Test(agent);
	}
	return false;
}