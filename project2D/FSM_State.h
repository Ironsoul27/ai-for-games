#pragma once
#include <vector>
#include <string>

using std::vector;

class Agent;
class Transition;


namespace aie
{
	class Renderer2D;
}

class State
{
public:
	State(const std::string& name);
	virtual ~State();

	// Get the name of the state.
	// Can be used for searching etc.
	const std::string& GetName() const { return m_Name; }

	virtual void Update(Agent* agent, float deltaTime) = 0;

	virtual void Init(Agent* agent) {};
	virtual void Exit(Agent* agent) {};
	virtual void Draw(Agent* agent, aie::Renderer2D* renderer);

	void AddTransition(Transition* transition);

	Transition* GetTriggeredTransition(Agent* agent);

protected:
	std::string				m_Name = "";
	vector<Transition*>		m_Transitons;
};


