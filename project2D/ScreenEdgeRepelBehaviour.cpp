#include "ScreenEdgeRepelBehaviour.h"
#include "Agent.h"
#include "Application.h"


bool ScreenEdgeRepelBehaviour::Update(Agent * agent, float deltaTime)
{
	float repelDistance = 20;

	if (agent->GetPosition().y < (m_App->getWindowHeight() - repelDistance)) {
		
		Vector2 force = agent->GetVelocity().normalised();
		float dist = m_App->getWindowHeight() - agent->GetPosition().y;

		Vector2 edgePosition = { (float)m_App->getWindowHeight(), agent->GetPosition().x };
		Vector2 agentPosition = agent->GetPosition();
		force = edgePosition - agentPosition;
		force.normalise();
		//force = force * 

		//agent->SetVelocity(force * ( dist / repelDistance) );
	}

	// Either
	// 1. Vector which gets added to our velocity to push us away in opposite direction

	// 2. Dividing velocity to ultimately reach 0

	// closer we get past the threshold the higher the vector2 repeling away from that edge // division of velocity

	// Requires agents position in worldspace
	return true;
}
