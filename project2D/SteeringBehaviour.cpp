#include "SteeringBehaviour.h"
#include "Agent.h"


SteeringBehaviour::SteeringBehaviour()
{
}

SteeringBehaviour::~SteeringBehaviour()
{
	for (SteeringForce* sf : m_Forces)
		delete sf;
}

bool SteeringBehaviour::Update(Agent* agent, float deltaTime)
{
	if (agent == nullptr)
		return false;
	
	if (agent->BlackboardDataExists("IsDead")) {
		agent->SetVelocity({ 0, 0 });
		return true;
	}

	Vector2 force = { 0, 0 };

	for (SteeringForce* sf : m_Forces) {
		force = force + sf->CalculateForce(agent, deltaTime);

		// If force has exceeded the agents maximum force then exit
		if (force >= agent->GetMaxForce())
		{
			force = agent->GetMaxForce();
			break;
		}
	}

	Vector2 forceOffset = force - agent->GetVelocity();
	Vector2 acceleration = forceOffset / agent->GetMass();

	const Vector2& vel = agent->GetVelocity();
	agent->SetVelocity(vel + (acceleration * deltaTime));

	return true;
}














/*if (force.x < agent->GetMaxForce().x)
	force.x = agent->GetMaxForce().x;
if (force.y < agent->GetMaxForce().y)
	force.y = agent->GetMaxForce().y;*/

	//force.clamp(agent->GetMaxForce().x);

	// Clamp to maximum vector range