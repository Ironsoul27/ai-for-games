#include "FSM_State.h"
#include "FSM_Transition.h"
#include "Agent.h"
#include "Renderer2D.h"

State::State(const std::string& name)
	: m_Name(name)
{
}


State::~State()
{
	for (auto t : m_Transitons)
		delete t;
}

void State::Draw(Agent * agent, aie::Renderer2D * renderer)
{
	renderer->setRenderColour(agent->GetColour().x, agent->GetColour().y, agent->GetColour().z);
	renderer->drawText(g_SystemFont, GetName().c_str(), agent->GetPosition().x + 10, agent->GetPosition().y + 10);
}

void State::AddTransition(Transition * transition)
{
	m_Transitons.push_back(transition);
}

Transition* State::GetTriggeredTransition(Agent * agent)
{
	// Loop over all the connected transitions & ask each if they are triggered.
	// If so, we'll return the triggered transition, so the FSM can use it to move states.
	for (auto transition : m_Transitons) 
	{
		if (transition->HasTriggered(agent))
			return transition;
	}

	return nullptr;
}
