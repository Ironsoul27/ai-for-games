#pragma once
#include "Behaviour.h"
#include "Pathfinding.h"
#include "SceneObject.h"
#include <list>

using namespace Pathfinding;

class FollowNodeTreeBehaviour :
	public Behaviour
{

//class SceneObject;

public:
	FollowNodeTreeBehaviour(NodeGraph2D& nodegraph, float range);
	virtual ~FollowNodeTreeBehaviour();

	virtual bool Update(Agent* agent, float deltaTime);

	bool GetNewWanderTrack();

protected:
	NodeGraph2D& m_NodeGraph;

	std::list<Node*> m_Path;
	std::list<Node*>::iterator m_Path_It;
	Node*		 m_TargetNode = nullptr;
	Node*		 m_CurrentNode = nullptr;
	Node*		 m_NextNode = nullptr;
	SceneObject* m_SeekObject = nullptr;
	bool		 m_UpdateTargetRequired = true;
	float		 m_CloseEnoughRange = 90;
	float		 m_CloseEnoughToTarget = 100;
};

