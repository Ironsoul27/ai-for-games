#pragma once
#include "Behaviour.h"
#include <vector>

class Agent;

using std::vector;

class FleeBehaviour : public Behaviour
{
public:
	FleeBehaviour();
	virtual ~FleeBehaviour();

	virtual bool Update(Agent* agent, float deltaTime);
	void SetTarget(Agent* agent) { m_Target = agent; }

protected:

	Agent* m_Target;
	//vector<Agent*> fleeAgents;
};

