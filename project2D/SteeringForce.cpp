#include "SteeringForce.h"
#include "Vector2.h"
#include "Application.h"
#include "Flock.h"
#include "Prey.h"
#include "Predator.h"
#include "Ray.h"
#include "Sphere.h"
#include "SceneObject.h"
#include <assert.h>
#include <iostream>


SteeringForce::SteeringForce()
{
}

SteeringForce::~SteeringForce()
{
}


//SeekForce::SeekForce(const std::string & blackboardPrefix)
//	: m_BlackboardPrefix(blackboardPrefix)
//{
//}

//Vector2 FleeForce::CalculateForce(Agent * agent, float deltaTime)
//{
//	if (agent->BlackboardDataExists("SeekOff") && agent->GetBlackboardData("SeekOff").BoolData ||
//		agent->GetState() == PlayerControlled)
//		return Vector2();
//
//	// Setup default values
//	Vector2	desiredForce = { 0, 0 };
//	Agent*	target = nullptr;
//	float	rangeOfInfluence = 1000;
//	float	seekSpeed = 100;
//
//	if (agent->GetFlock().FlockBlackboardDataExists("PackTarget")) 
//		target = agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData;
//
//	if (target != nullptr) 
//	{
//		const Vector2& agentPosition = agent->GetPosition();		
//		const Vector2& targetPosition = target->GetPosition();
//
//		const Vector2& dir = targetPosition - agentPosition;
//		float magSqr = (dir.x * dir.x + dir.y * dir.y);
//
//		if (magSqr > 0 && magSqr < (rangeOfInfluence*rangeOfInfluence))
//		{
//			float mag = sqrt(magSqr);
//			desiredForce = dir * (seekSpeed / mag);
//		}
//	}
//
//	return desiredForce;
//}


WanderForce::WanderForce(aie::Application * app, vector<SceneObject*>& sceneObjects)
	: m_App(app), m_SceneObjects(sceneObjects)
{
}

void WanderForce::RecalculateTarget()
{
	m_CalculateTargetRequired = true;
}


void WanderForce::updateWanderTarget(Agent * agent)
{
	if (agent->BlackboardDataExists("WanderOn") && !agent->GetBlackboardData("WanderOn").BoolData)
		return;

	// Set default values
	float wanderRadius = 50;
	float wanderDistance = 200;

	// Check Blackboard data for any settings
	if (agent->BlackboardDataExists("WanderRadius"))
		wanderRadius = agent->GetBlackboardData("WanderRadius").FloatData;

	if (agent->BlackboardDataExists("WanderDistance"))
		wanderDistance = agent->GetBlackboardData("WanderDistance").FloatData;

	Vector2 vRandom = { getRandomValue(), getRandomValue() };

	// Normalise vRandom
	float mag = sqrt(vRandom.x * vRandom.x + vRandom.y * vRandom.y);
	vRandom = vRandom / mag;

	// Multply vRandom by our wander radius to get a random position on the Wander Circle.
	Vector2 pos = vRandom * wanderRadius;

	// Get the agent's current velocity (ie direction its travelling)
		// We'll offset the 'pos' in that direction by the Wander Distance
	Vector2 agentDir = agent->GetVelocity();

	// Normalise the Agent Direction
	mag = sqrt(agentDir.x * agentDir.x + agentDir.y * agentDir.y);
	if (mag > 0)
	{
		agentDir = agentDir / mag;
	}

	// Then move the pos by the AgentDir, scaled up to the WanderDistance
	pos = pos + agentDir * wanderDistance;

	// Add Agent's original position to the offset
	pos += agent->GetPosition();

	// If overlapping with any colliders adjust position to outside of the collision radius
	// --ISSUE-- If point overlaps a collider that was already checked after the point was moved to avoid another collision it will continue to stay inside that collider
	for (auto& SO : m_SceneObjects)
	{
		Sphere& collider = *SO->GetCollider();
		if (collider.overlaps(pos))
		{
			Vector2& avoidanceForce = pos - collider.center;

			avoidanceForce = avoidanceForce * ((collider.radius * collider.radius) / avoidanceForce.magnitudeSqr());

			pos += avoidanceForce;
		}
	}

	// Clamp on screen bounds
	if (pos.x < 0)
		pos.x = 0;
	else if (pos.x > m_App->getWindowWidth())
		pos.x = (float)m_App->getWindowWidth();
	if (pos.y < 0)
		pos.y = 0;
	else if (pos.y > m_App->getWindowHeight())
		pos.y = (float)m_App->getWindowHeight();

	

	// Set the new position that the Wander should seek towards on the Agent blackboard.
	agent->GetBlackboardData("WanderTarget").Vector2Data = pos;
}

float WanderForce::getRandomValue()
{
	return ((float)rand() / (float)RAND_MAX) - 0.5f;
}



Vector2 WanderForce::CalculateForce(Agent * agent, float deltaTime)
{
	// If agent is in wander state break and calculate the force
	switch (agent->GetState()) {
		case Wander: {
			break;
		}
		default:
			return Vector2();
	}


	// Init default values
	Vector2 desiredForce = { 0, 0 };
	Vector2 wanderTarget = agent->GetPosition();
	float wanderSpeed = 100;
	float closeEnoughDistance = 10;
	float wanderWeight = .3f;


	// Update the Wander Target, as required
	if (m_CalculateTargetRequired)
	{
		updateWanderTarget(agent);
		m_CalculateTargetRequired = false;
	}

	// Check the blackboard for any settings
	if (agent->BlackboardDataExists("WanderTarget")) // Change to get the flocks wander target
		wanderTarget = agent->GetBlackboardData("WanderTarget").Vector2Data;

	if (agent->BlackboardDataExists("WanderSpeed"))
		wanderSpeed = agent->GetBlackboardData("WanderSpeed").FloatData;

	if (agent->BlackboardDataExists("WanderCloseEnoughDistance"))
		closeEnoughDistance = agent->GetBlackboardData("WanderCloseEnoughDistance").FloatData;

	if (agent->BlackboardDataExists("WanderWeight"))
		wanderWeight = agent->GetBlackboardData("WanderWeight").FloatData;

	// Seek to the target
	const Vector2& agentPosition = agent->GetPosition();

	Vector2 dir = wanderTarget - agentPosition;
	float magSqr = (dir.x * dir.x + dir.y * dir.y);

	if (magSqr > 0)
	{
		float mag = sqrt(magSqr);
		desiredForce = dir * (wanderSpeed / mag);	// Normalising, then scaling by speed
	}

	// Am I close enough to the target? 
	// If so, then update the wanderTarget
	if (magSqr < (closeEnoughDistance * closeEnoughDistance))
	{
		updateWanderTarget(agent);
	}

	return (desiredForce * wanderWeight);
}


//AlignmentForce::AlignmentForce(vector<Agent*>& agents, std::list<Agent*>& neighbourhoodList)
//	: m_Flock(agents), m_Neighbourhood(neighbourhoodList)
//{
//}
//
//Vector2 AlignmentForce::CalculateForce(Agent * agent, float deltaTime)
//{
//	//if (agent->BlackboardDataExists("WanderOff") && agent->GetBlackboardData("WanderOff").BoolData)
//	//	return Vector2();
//
//	Vector2		desiredForce = { 0, 0 };
//	Vector2		alignForce = { 0, 0 };
//	float		alignmentWeight = 4.0f;
//
//	if (agent->BlackboardDataExists("AlingmentWeight"))
//		alignmentWeight = agent->GetBlackboardData("AlingmentWeight").FloatData;
//
//	for (auto& a : m_Neighbourhood)
//	{
//		
//		Vector2 v = a->GetVelocity();
//		v.normalise();
//		if (a == agent->GetFlock().GetFlockLeader())
//		{
//			v = v * 5;
//		}
//		alignForce = alignForce + v;
//	}
//
//	//alignForce = alignForce / (float)m_Neighbourhood.size();
//
//	return ((alignForce - agent->GetVelocity()) * alignmentWeight);
//}
//
//
//
//CohesionForce::CohesionForce(vector<Agent*>& agents, std::list<Agent*>& neighbourhoodList)
//	: m_Flock(agents), m_Neighbourhood(neighbourhoodList)
//{
//}

Vector2 CohesionForce::CalculateForce(Agent * agent, float deltaTime)
{
	Vector2		cohesionForce = { 0, 0 };
	float		m_cohesionWeight = 4.0f;

	if (agent->BlackboardDataExists("CohesionWeight"))
		m_cohesionWeight = agent->GetBlackboardData("CohesionWeight").FloatData;

	switch (agent->GetState()) {
	case EatTarget: 
	{
		Agent* target = agent->GetFlock().GetFlockBlackboardData("EatTarget").AgentData;
		cohesionForce = cohesionForce + target->GetPosition() - agent->GetPosition();
		break;
	}
	default: 
	{ return Vector2(); }
	}

	return ((cohesionForce - agent->GetVelocity()) * m_cohesionWeight);
}
//
//
//
//FlockForces::FlockForces(vector<Agent*>& agents)
//	: m_Flock(agents)
//{
//}
//
//FlockForces::~FlockForces()
//{
//	for (auto& flockForce : m_FlockForces)
//	{
//		delete flockForce;
//	}
//}
//
//
//
//// Calculate Flocking Forces
//Vector2 FlockForces::CalculateForce(Agent * agent, float deltaTime)
//{
//	Vector2 desiredForce;
//
//	calculateNeighbourhood(agent);
//
//	for (auto& flockForce : m_FlockForces)
//	{
//		desiredForce += flockForce->CalculateForce(agent, deltaTime);
//	}
//	
//	// clear neighbourlist for recalculation
//	m_Neighbourhood.clear();
//
//	return desiredForce;
//}
//
//
//// Calculate Neighbouring Agents
//void FlockForces::calculateNeighbourhood(Agent* agent)
//{
//	float neighbourhoodDistance = 10000;
//
//	for (auto& a : m_Flock)
//	{
//		Vector2 facing = a->GetPosition() - agent->GetPosition();
//		float magSqr = facing.magnitudeSqr();
//
//		if (magSqr < neighbourhoodDistance)
//		{
//			m_Neighbourhood.push_back(agent);
//		}
//	}
//}


// Calculate SeekFlee Force
Vector2 SeekFleeForce::CalculateForce(Agent * agent, float deltaTime) 
{
	Agent*		 target = nullptr;
	Vector2		 desiredForce;
	Vector2		 leaderFollowPoint;
	Vector2		 maximumForce = { 300, 300 };
	float		 slowingRadius = 1000;
	float		 speed = 1.0f;

	switch (agent->GetState()) {

		// Chase target
		case SeekPrey: {
			slowingRadius = 200;
			speed = 1.7f;

			// Assign target if we have one
			if (agent->GetFlock().FlockBlackboardDataExists("PackTarget")) 
				target = agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData;

			// If we have a target
			if (target != nullptr)
				desiredForce = target->GetPosition() - agent->GetPosition();
			else
				return Vector2();

			break;
		}

		// Follow node tree target
		case NodeTreeTarget: {
			slowingRadius = 100;
			const Vector2* targetPos = nullptr;

			if (agent->BlackboardDataExists("SeekTarget"))
				targetPos = &agent->GetBlackboardData("SeekTarget").Vector2Data;
			
			if (targetPos != nullptr)
				desiredForce = *targetPos - agent->GetPosition();
			else
				return Vector2();

			break;
		}

		// Run from targets
		case Flee: {
			speed = 50;

			if (agent->BlackboardDataExists("FleeSpeed"))
				speed = agent->GetBlackboardData("FleeSpeed").FloatData;

			Prey* prey = dynamic_cast<Prey*>(agent);
			if (prey != nullptr)
			{
				Flee_Agents_Map* FleeFlocks = &prey->GetFlock().GetFleeFlocks();
				Flee_Agents_Map::iterator it;
				unsigned int numFleeMembers = 0;

				// Iterate over flocks
				for (it = FleeFlocks->begin(); it != FleeFlocks->end(); it++)
				{
					vector<Agent*>* v_ptr = &it->second.first.GetFlockAgents();
					numFleeMembers += (unsigned int)v_ptr->size();

					// Iterate over agents
					for (auto& a : *v_ptr)
					{
						desiredForce += agent->GetPosition() - a->GetPosition();
					}
				}
				desiredForce = desiredForce / (float)numFleeMembers;
				desiredForce.normalise();
				desiredForce = desiredForce * speed;
			}
			break;
		}

		// Follow flock leader
		case FollowLeader: {
			slowingRadius = 1000;

			// If we have a leader follow point  &&  agent is not the Flock Leader
			if (agent->GetFlock().FlockBlackboardDataExists("LeaderFollowPoint") &&
				agent != agent->GetFlock().GetFlockLeader())
			{
				leaderFollowPoint = agent->GetFlock().GetFlockBlackboardData("LeaderFollowPoint").Vector2Data;
				desiredForce = leaderFollowPoint - agent->GetPosition();
			}
			break;
		}
		default: {
			return Vector2();
		}
	}


	/////////////  APPLY FORCES  ////////////////
	
	float magSqr = desiredForce.magnitudeSqr();

	// If agent is within slow down radius reduce force
	if (magSqr < slowingRadius)
		desiredForce = (desiredForce.normalised() *= maximumForce) * (magSqr / slowingRadius);
	// Else apply standard seek/flee force
	else 
		desiredForce = desiredForce.normalised() *= maximumForce;
	
	desiredForce = desiredForce * speed;

	return (desiredForce - agent->GetVelocity());
}


Vector2 SeperationForce::CalculateForce(Agent * agent, float deltaTime)
{
	Vector2 desiredForce;
	float   neighborhoodCount = 0;
	float	seperationRadius = 140;
	float	maxSeperation = 140;

	switch (agent->GetState()) {
		case EatTarget: {
			maxSeperation = 300;

			if (agent == agent->GetFlock().GetFlockBlackboardData("HungriestMember").AgentData)
				return Vector2();

			Agent* target = agent->GetFlock().GetFlockBlackboardData("EatTarget").AgentData;

			desiredForce = agent->GetPosition() - target->GetPosition();
			desiredForce.normalise();
			desiredForce = desiredForce * maxSeperation;

			return desiredForce;
			break;
		}
		case Wander: {
			if (agent == agent->GetFlock().GetFlockLeader())
				return Vector2();
			break;
		}
		case Flee: {
			return Vector2();
			break;
		}
	}
	
	// Loops over agents in flock and checks if within seperationradius, if so they contribute to the force

	vector<Agent*>* FlockAgents = &agent->GetFlock().GetFlockAgents();

	for (auto& a : *FlockAgents)
	{
		if (a->GetPosition().distance(agent->GetPosition()) <= seperationRadius)
		{
			desiredForce = desiredForce + (a->GetPosition() - agent->GetPosition());
			neighborhoodCount++;
		}
	}

	// If we had neighbours that were included in calculation
	if (neighborhoodCount != 0) {
		desiredForce = desiredForce / neighborhoodCount;
		desiredForce = desiredForce * -1;
	}
	else
		return Vector2();
		
	desiredForce.normalise();
	desiredForce = desiredForce * maxSeperation;

	return desiredForce;
}


CollisionAvoidance::CollisionAvoidance(std::vector<SceneObject*>& sceneObjectGroup)
	: m_sceneObjects(sceneObjectGroup)
{
}

Vector2 CollisionAvoidance::CalculateForce(Agent* agent, float deltaTime) {
	Vector2 desiredForce;
	Vector2 MAX_AVOID_FORCE = { 3000, 3000 };
	float MAX_VELOCITY = 400;
	float MAX_SEE_AHEAD = 20;

	// Dynamic ray length to scale with agents velocity
	float dynamicLength = agent->GetVelocity().magnitudeSqr() / MAX_VELOCITY;

	const Vector2& normalisedVel = agent->GetVelocity().normalised();
	
	// Generate a ray ahead of agents to detect collisions
	Ray ahead(agent->GetPosition(), normalisedVel, dynamicLength);

	agent->GetBlackboardData("CollisionPoints").Vector2Data = (agent->GetPosition() + normalisedVel * dynamicLength);
	agent->GetBlackboardData("CollisionPoints").RayData = ahead;

	// Return most threatening collider
	Sphere* mostThreatening = FindMostThreatining(agent, ahead);

	// If we have collider
	if (mostThreatening != nullptr)
	{
		desiredForce = agent->GetPosition() - mostThreatening->center;

		desiredForce.normalise();
		desiredForce *= MAX_AVOID_FORCE;
	}
	else
		return Vector2();

	return desiredForce;
}


Sphere* CollisionAvoidance::FindMostThreatining(Agent* agent, Ray& ahead)
{
	Sphere* mostThreatening = nullptr;

	for (auto& SO : m_sceneObjects)
	{
		Sphere& Collider = *SO->GetCollider();
		bool collision = ahead.intersects(Collider);

		// If we have a collision  &&  either mostThreatening hasn't been set  ||  the distance between the new sphere is closer than our current mostThreatening sphere
		if (collision && (mostThreatening == nullptr || agent->GetPosition().distance(Collider.center) < agent->GetPosition().distance(mostThreatening->center)))
			mostThreatening = &Collider;
	}

	return mostThreatening;
}
