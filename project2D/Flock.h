#pragma once
#include "Agent.h"
#include "Flock.h"
#include <unordered_map>
#include <string>
#include <list>
#include <utility>

using std::string;

typedef std::pair<Flock, bool> Flock_Pair;
typedef std::unordered_map < std::string, Flock_Pair> Flee_Agents_Map;

class Flock : public Agent::BlackboardData
{
public:

	friend class Agent;
	Flock() {}
	Flock(const std::string& id) { m_FlockId = id; }
	virtual ~Flock() {}

	// Id & Register
	const void		   Register(Agent* agent) { m_Agents.push_back(agent); }
	vector<Agent*>&	   GetFlockAgents() { return m_Agents; }
	void			   SetId(const std::string& id) { m_FlockId = id; }
	std::string& GetId() { return m_FlockId; }

	// Leader Functions
	Agent*&			    GetFlockLeader() { return m_Leader; }
	void				SetFlockLeader(Agent* agent) { m_Leader = agent; }
	bool				IsLeader(Agent* agent) { return agent == m_Leader; }

	Agent*&				GetLastMember() { return m_Agents.back(); }

	// Hunger Functions
	std::unordered_map<string, Agent*>&		GetHungryMembers() { return m_HungryMembers; }
	bool				IsFLockMemberHungry() { return (m_HungryMembers.size() != 0); }
	const void			AddHungryMember(Agent* agent) { m_HungryMembers.insert({agent->GetName() ,agent }); }
	void				RemoveHungryMember(const std::string& id) { m_HungryMembers.erase(id); }


	// Flee flock functions
	Flock&				GetFleeFlock(const std::string& id) { return m_FleeAgents.at(id).first; }
	Flee_Agents_Map&	GetFleeFlocks() { return m_FleeAgents; }
	void				AddFleeFlock(std::string& id, Flock& flock)
	{
		Flock_Pair fp = std::make_pair(flock, true);
		m_FleeAgents.insert({ id, fp });
	}
	const void			RemoveFleeFlock(const std::string& id) { m_FleeAgents.erase(id); }
	bool				IsFlockInList(const std::string& id) { return m_FleeAgents.count(id); }
	void				SetCanBeSeen(const std::string& id, const bool canSee) { m_FleeAgents.at(id).second = canSee; }
	Flee_Agents_Map&	GetFleeAgentList() { return m_FleeAgents; }

	// Flock Blackboard
	std::unordered_map<string, BlackboardData>&	GetFlockBlackboard() { return m_FlockBlackboard; }
	bool				FlockBlackboardDataExists(const std::string& id) const { return m_FlockBlackboard.count(id); }
	BlackboardData&		GetFlockBlackboardData(const std::string id) { return m_FlockBlackboard[id]; }
	void				ClearFlockBlackboardData(const std::string& id) { m_FlockBlackboard.erase(id); }

protected:
	Agent*										m_Leader;
	std::string									m_FlockId;
	vector<Agent*>								m_Agents;
	std::unordered_map<string, BlackboardData>  m_FlockBlackboard;
	std::unordered_map<string, Agent*>			m_HungryMembers;
	Flee_Agents_Map								m_FleeAgents;
};

