#include "ChaseState.h"
#include <iostream>
#include "Agent.h"



ChaseState::ChaseState(Agent* target)
	: State("Chase"),  m_Target(target)
{
}


ChaseState::~ChaseState()
{
}

void ChaseState::Update(Agent * agent, float deltaTime)
{
}

void ChaseState::Init(Agent * agent)
{
	std::cout << "[ChaseState::Init]\n";

	// Set up blackboard data for the WanderForce, if its attached to the Agent
	agent->GetBlackboardData("SeekTarget").AgentData = m_Target;
	agent->GetBlackboardData("SeekSpeed").FloatData = 100;
}

void ChaseState::Exit(Agent * agent)
{
	std::cout << "[ChaseState::Exit]\n";

	// Stop the SeekForce from applying any forces by setting the target to nullptr
	agent->ClearBlackboardData("SeekTarget");
}
