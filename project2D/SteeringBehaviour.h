#pragma once
#include "Behaviour.h"
#include "SteeringForce.h"
#include <vector>

using std::vector;



class SteeringBehaviour : public Behaviour

{
public:

	SteeringBehaviour();
	~SteeringBehaviour();

	void AddSteeringForce(SteeringForce* force) ///////
	{ 
		m_Forces.push_back(force);
	}

	virtual bool Update(Agent* agent, float deltaTime) override;

protected:

	vector<SteeringForce*> m_Forces;
};

