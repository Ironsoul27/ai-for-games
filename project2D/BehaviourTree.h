#pragma once
#include "IBehaviour.h"
#include "Behaviour.h"
#include <iostream>
#include <list>

using std::cout;
using std::endl;

class Agent;
class Composite;

class BehaviourTree : public Behaviour
{
public:
	BehaviourTree(Composite* behaviour);
	virtual ~BehaviourTree();

	virtual bool Update(Agent* agent, float deltaTime) override;

protected:
	IBehaviour* m_Root;
};


class InverseDecorator : public IBehaviour
{
public:
	InverseDecorator(IBehaviour* node) { m_Child = node; }
	virtual ~InverseDecorator() { delete m_Child; }
	virtual BehaviourResult execute(Agent* agent, float deltaTime);
	void					AddChild(IBehaviour* node) { m_Child = node; }

protected:
	IBehaviour* m_Child; 
};

class LogDecorator : public IBehaviour
{
public:
	LogDecorator(IBehaviour* node, const std::string& message) { m_Child = node;
																 m_Message = message;
	}

	virtual BehaviourResult execute(Agent* agent, float deltaTime);
	void					AddChild(IBehaviour* node) { m_Child = node; }
	const void				SetMessage(const std::string& message) { m_Message = message; }

protected:
	IBehaviour*	 m_Child;
	std::string	 m_Message;
};


class Composite : public IBehaviour
{
public:
	virtual ~Composite()
	{
		for (auto b : m_Children)
		{
			delete b;
		}
	}

	virtual BehaviourResult execute(Agent* agent, float deltaTime) = 0;
	void const AddBehaviour(IBehaviour* behaviour);

protected:
	std::list<IBehaviour*> m_Children;
};


class Sequence : public Composite
{
public:
	virtual ~Sequence() {}
	virtual BehaviourResult execute(Agent* agent, float deltaTime);

};


class Selector : public Composite
{
public:
	virtual ~Selector() {}
	virtual BehaviourResult execute(Agent* agent, float deltaTime);

};
