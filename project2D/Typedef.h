#pragma once
#include <utility>
#include <vector>
#include <unordered_map>
#include <string>
#include "Agent.h"

//class Agent;
//class BlackboardData;


typedef std::unordered_map<std::string, Agent::BlackboardData> BlackBoard_Shared_Ptr;
typedef std::unordered_map<std::string, std::vector<Agent*>> Agent_UOM;