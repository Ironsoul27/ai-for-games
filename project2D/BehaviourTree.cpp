#include "BehaviourTree.h"
#include "Agent.h"




BehaviourTree::BehaviourTree(Composite* behaviour)
	: m_Root(behaviour)
{}

BehaviourTree::~BehaviourTree()
{ 
	delete m_Root;
}


bool BehaviourTree::Update(Agent * agent, float deltaTime)
{
	if (m_Root->execute(agent, deltaTime) == Success || Ongoing)
		return true;

	return false;
}


// AND Node // All conditions must return true to return true
BehaviourResult Sequence::execute(Agent * agent, float deltaTime)
{
	for (auto& child : m_Children)
	{
		if (child->execute(agent, deltaTime) == Failure)
			return Failure;
	}
	return Success;
}


// OR Node // One condition must return true to return true
BehaviourResult Selector::execute(Agent * agent, float deltaTime)
{
	for (auto& child : m_Children)
	{
		if (child->execute(agent, deltaTime) == Success)
			return Success;
	}
	return Failure;
}

// Set data through blackboard when condition returns success, that way the SteeringBehaviours can influence their respective agents


void const Composite::AddBehaviour(IBehaviour * behaviour)
{
	m_Children.push_back(behaviour);
}


BehaviourResult InverseDecorator::execute(Agent * agent, float deltaTime)
{
	BehaviourResult result = m_Child->execute(agent, deltaTime);
	switch (result)
	{
		case Success: return Failure;
		case Failure: return Success;
	}
	return Failure;
}

BehaviourResult LogDecorator::execute(Agent * agent, float deltaTime)
{
	cout << m_Message << endl;
	return m_Child->execute(agent, deltaTime);
}
