#pragma once
#include "Vector2.h"
#include "Sphere.h"
#include "Texture.h"
#include "Renderer2D.h"


class SceneObject
{
public:
	SceneObject() { m_Collider.radius = 0; }
	virtual ~SceneObject() {}

	virtual void	onDraw(aie::Renderer2D* renderer) {}
	virtual void	onUpdate(float deltaTime) {}

	void Update(float deltaTime) {
		onUpdate(deltaTime);
		m_Collider.center = m_Position;
	}

	void Draw(aie::Renderer2D* renderer) {
		onDraw(renderer);
		// Draw debug info if required
	}

	Vector2&		GetPosition() { return m_Position; }
	void const		SetPosition(const Vector2& pos) { m_Position = pos; }
	Sphere*			GetCollider() { return &m_Collider; }

protected:

	Vector2 m_Position;
	Sphere  m_Collider;

};

