#include "UpdateNecessities.h"
#include "Animal.h"
#include "Agent.h"
#include "Flock.h"
#include "Predator.h"
#include "Prey.h"
#include <assert.h>

// Update necessities (IE. Hunger, Thirst)
bool UpdateNecessities::Update(Agent * agent, float deltaTime)
{
	// If agent is dead stop update
	if (agent->BlackboardDataExists("IsDead") && agent->GetBlackboardData("IsDead").BoolData == true)
		return true;

	m_Timer += deltaTime;

	assert(agent != nullptr);

	float hungerThreshold = 20;

	if (agent->BlackboardDataExists("HungerThreshold"))
		hungerThreshold = agent->GetBlackboardData("HungerThreshold").FloatData;

	//Predator* predator = dynamic_cast<Predator*>(agent);
	//if (predator != nullptr)
	//{
	//	// Maybe change to checks which say which state we are in instead of just if we have a target or not
	//	// Add enum to animal class, with protected values which derived classes can access

	//	// If we have a pack target then set the seperation values lower so predators can attack target
	//	//if (predator->GetFlock().FlockBlackboardDataExists("PackTarget")) {

	//	//	// Chase
	//	//	predator->GetBlackboardData("MaxSeperation").FloatData = 40;
	//	//	predator->GetBlackboardData("SeperationRadius").FloatData = 40;
	//	//}
	//	//else {
	//	//	// Wander
	//	//	predator->GetBlackboardData("MaxSeperation").FloatData = 140;
	//	//	predator->GetBlackboardData("SeperationRadius").FloatData = 140;
	//	//}
	//}

	// Prey Flee Flocks

	// UpdateNeccessities must check to update the agents list of agents to watch out for, although really they should be linked to the same list, 
	// perhaps during prey creation we can link the 2 lists to point to the same list, so flock can hold functions instead of individual agents

	Animal* animal = dynamic_cast<Animal*>(agent);
	if (animal != nullptr)
	{
		// Unneccessary to set here, should be done in each respective force   // Method to only set once when change state is detected
		//switch (agent->GetState()) {
		//	case SeekPrey: {
		//		if (animal->BlackboardDataExists("PackTarget")) {
		//			animal->GetBlackboardData("MaxSeperation").FloatData = 40;
		//			animal->GetBlackboardData("SeperationRadius").FloatData = 40;
		//		}
		//		break;
		//	}
		//	case Flee: {


		//		break;
		//	}
		//	case Wander: {
		//		break;
		//	}
		//	case FollowLeader: {
		//		animal->GetBlackboardData("MaxSeperation").FloatData = 200;
		//		animal->GetBlackboardData("SeperationRadius").FloatData = 200;
		//		break;
		//	}
		//	default: {
		//		//throw ("UpdateNeccessities -- Switch state returned null");
		//	}			
		//}

		// Try and re go-over why local targets where neccesary
		 
		 // If flock has a target set the agents local target to the flocks
		//if (animal->GetFlock().FlockBlackboardDataExists("PackTarget") &&  // If the flock has a target  &&  This agent's target isn't already the flock target
		//	animal->BlackboardDataExists("PackTarget") &&
		//	animal->GetBlackboardData("PackTarget").AgentData != animal->GetFlock().GetFlockBlackboardData("PackTarget").AgentData/* &&
		//	animal->GetFlock().FlockBlackboardDataExists("HungriestMember") &&
		//	animal != animal->Get*/)
		//{
		//	animal->GetBlackboardData("PackTarget").AgentData = animal->GetFlock().GetFlockBlackboardData("PackTarget").AgentData;
		//}


		// If animal is hungry and hasn't already been labelled as hungry
		if (animal->GetHunger() <= hungerThreshold  &&  animal->BlackboardDataExists("Hungry")  &&  animal->GetBlackboardData("Hungry").BoolData == false)
		{
			animal->GetBlackboardData("Hungry").BoolData = true;
			animal->GetFlock().AddHungryMember(animal);
		}
		else if (animal->GetHunger() > hungerThreshold  &&  animal->BlackboardDataExists("Hungry")  &&  animal->GetBlackboardData("Hungry").BoolData == true)
		{
			animal->GetBlackboardData("Hungry").BoolData = false;
			animal->GetFlock().RemoveHungryMember(animal->GetName());
		}

		if (m_Timer > 5.f) {

			float Hunger_Depletion_Rate = 1;
			float Thirst_Depletion_Rate = 1;
			float Tiredness_Depletion_Rate = 1;

			if (agent->BlackboardDataExists("HungerDepletionRate"))
				Hunger_Depletion_Rate = agent->GetBlackboardData("HungerDepletionRate").FloatData;

			if (agent->BlackboardDataExists("ThirstDepletionRate"))
				Thirst_Depletion_Rate = agent->GetBlackboardData("ThirstDepletionRate").FloatData;

			if (agent->BlackboardDataExists("TirednessDepletionRate"))
				Tiredness_Depletion_Rate = agent->GetBlackboardData("TirednessDepletionRate").FloatData;

			animal->SetHunger((animal->GetHunger() - Hunger_Depletion_Rate));
			animal->SetThirst((animal->GetThirst() - Thirst_Depletion_Rate));
			animal->SetTiredness((animal->GetTiredness() - Tiredness_Depletion_Rate));

			// Last member calculates hungriest member after timer update
			if (agent == agent->GetFlock().GetLastMember()) 
				CalculateHungriestMember(agent);
			
			m_Timer = 0;
			return true;
		}
	}
	else
		return false;

	return true;
}

void const UpdateNecessities::CalculateHungriestMember(Agent* agent)
{
	vector<Agent*>* AgentFlock = &agent->GetFlock().GetFlockAgents();
	Agent* hungriestMember;
	float  lowestHunger = FLT_MAX;

	for (auto& a : *AgentFlock)
	{
		Animal* animal = dynamic_cast<Animal*>(a);
		if (animal != nullptr)
		{
			if (animal->GetHunger() < lowestHunger) {
				lowestHunger = animal->GetHunger();
				hungriestMember = a;
			}
		}
	}

	agent->GetFlock().GetFlockBlackboardData("HungriestMember").AgentData = hungriestMember;
}