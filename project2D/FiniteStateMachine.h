#pragma once
#include "Behaviour.h"
#include <vector>

using std::vector;

class State;
class Condition;
class Transition;


// The main Finite State Machine which maintains a collection of states.
// Uses transitions to move between states.
// Each transition has a condition attached to determine when the transition is 'active' or 'triggered'

class FiniteStateMachine : public Behaviour
{
public:
	FiniteStateMachine();
	virtual ~FiniteStateMachine();

	// Add a State to FSM, takes ownership
	State* AddState(State* state, bool makeCurrent = false);

	// Change the current state on the next update
	void	SetScheduledState(State* state);

	State*	FindState(const std::string& name) const;


	//Transition* AddTransition(Transition* transition);
	//Condition* addCondition(Condition* condition);
	//void setCurrentState(State* state);

	virtual bool Update(Agent* agent, float deltaTime) override;
	virtual void Draw(Agent* agent, aie::Renderer2D* renderer) override;




protected:
	vector<State*>			m_States;

	State*					m_CurrentState = nullptr;

	State*					m_ScheduledState = nullptr;

	//vector<Transition*>		m_Transitions;
	//vector<Condition*>		m_Conditions;
};
