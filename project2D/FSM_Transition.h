#pragma once

class State;
class Agent;
class Condition;

// Base class to hold information for transitioning between states
class Transition
{
public:
	// Constructor takes ownership of the input Condition
	Transition(State* target, Condition* condition) : m_Target(target), m_Condition(condition) {};
	virtual ~Transition();

	State* GetTargetState();

	// Returns true if the transition has been triggered (using the m_Condition)
	bool HasTriggered(Agent* agent);


protected:

	State* m_Target;
	Condition* m_Condition;
};