#pragma once
#include "AnimalStates.h"

class Agent;

namespace aie
{
	class Renderer2D;
}

class Behaviour
{
public:
	Behaviour() {}
	virtual ~Behaviour() {}

	virtual bool Update(Agent* agent, float deltaTime) = 0;
	virtual void Draw(Agent* agent, aie::Renderer2D* renderer) {}; // default does nothing

	//virtual void SetBehaviourWeight() = 0;
};
