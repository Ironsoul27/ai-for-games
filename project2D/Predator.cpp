#include "Predator.h"
#include "Application.h"
#include "KeyboardBehaviour.h"
#include "SteeringBehaviour.h"
#include "DebugBehaviour.h"
#include "DragBehaviour.h"
#include "WrapScreenBehaviour.h"
#include "UpdateNecessities.h"
#include "FollowLeaderBehaviour.h"
#include "FollowNodeTreeBehaviour.h"
//#include "Pathfinding.h"
#include "BehaviourTree.h"
#include "BT_Actions.h"
#include "BT_Conditions.h"
#include "Flock.h"

using namespace aie;


Predator::Predator(Flock& sharedFlock, const std::string & name, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph)
	: Animal(sharedFlock, name)
{
	Init(targetAgents, agentwatchlist, app, sceneObjects, nodegraph);
}

Predator::Predator(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector3& col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph)
	: Animal(sharedFlock, name, pos, col)
{ 
	Init(targetAgents, agentwatchlist, app, sceneObjects, nodegraph);
}

Predator::Predator(Flock& sharedFlock, const std::string & name, const Vector2 & pos, const Vector2 & vel, const Vector3 & col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph)
	 : Animal(sharedFlock, name, pos, vel, col)
{
	Init(targetAgents, agentwatchlist, app, sceneObjects, nodegraph);
}

Predator::~Predator()
{
}

const void Predator::Init(Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects, NodeGraph2D& nodegraph)
{
	m_App = app;

	AddBehaviour(new UpdateNecessities());

	// Add the SteeringBehaviours to the Predator
	SteeringBehaviour* sb = new SteeringBehaviour();
	sb->AddSteeringForce(new SeekFleeForce());
	sb->AddSteeringForce(new CollisionAvoidance(sceneObjects));
	sb->AddSteeringForce(new SeperationForce()); // Make global and check if agent is leader and if so don't apply forces, perhaps return if in wander behaviour and agent is flock leader
	sb->AddSteeringForce(new CohesionForce());


	// If the agent is the leader implement leader specific behaviours
	if (GetName().compare("Predator 1") == 0)
	{
		m_Flock.SetFlockLeader(this);

		sb->AddSteeringForce(new WanderForce(app, sceneObjects));

		AddBehaviour(new FollowLeaderBehaviour());
		AddBehaviour(new DebugBehaviour());
		AddBehaviour(new KeyboardBehaviour());
		AddBehaviour(new FollowNodeTreeBehaviour(nodegraph, 90));

		GetBlackboardData("WanderSpeed").FloatData = 40;
		GetBlackboardData("WanderRadius").FloatData = 50;
		GetBlackboardData("WanderDistance").FloatData = 10;
		GetBlackboardData("WanderWeight").FloatData = 1.f;

		//m_StateData = Wander;
		m_StateData = NodeTreeTarget;
	}
	else { // Else apply seperate forces to every member besides the leader
		
		m_StateData = FollowLeader;
	}

	AddBehaviour(sb);

	// Base Selector
	Selector* Base_Sel = new Selector();

	// IS PREDATOR HUNGRY SELECTOR
	Selector* Hungry_Predator_Sel = new Selector();
	Sequence* Reset_Hungry_Status_Seq = new Sequence();

	Reset_Hungry_Status_Seq->AddBehaviour(new InverseDecorator(new IsFlockMemberHungry()));
	Reset_Hungry_Status_Seq->AddBehaviour(new ClearFlockTarget());

	// Find Prey Sequence
	Sequence* FindPrey_Seq = new Sequence();
	FindPrey_Seq->AddBehaviour(new IsFlockMemberHungry());
	FindPrey_Seq->AddBehaviour(new InRangeCheck(targetAgents, agentwatchlist, 120));

	// Check To Remove Prey
	// FIND PREY SEQUENCE
	Sequence* AttackOrRemovePrey_Seq = new Sequence();

	// ATTACK / REMOVE TARGET SELECTOR
	Selector* Attack_RemoveTarget_Sel = new Selector();

	// ATTACK SEQUENCE
	Sequence* Attack_Seq = new Sequence();
	Attack_Seq->AddBehaviour(new InverseDecorator(new IsTargetDead()));
	Attack_Seq->AddBehaviour(new CloseEnoughToAttack());
	Attack_Seq->AddBehaviour(new AttackTarget());


	// REMOVE TARGET SEQUENCE
	Sequence* RemovePrey_Seq = new Sequence();
	RemovePrey_Seq->AddBehaviour(new IsLastMember());
	RemovePrey_Seq->AddBehaviour(new CanFlockNotSeeTarget());
	RemovePrey_Seq->AddBehaviour(new ClearFlockTarget());

	// DEAD TARGET SEQUENCE
	Sequence* DeadTarget_Seq = new Sequence();
	DeadTarget_Seq->AddBehaviour(new IsTargetDead());
	DeadTarget_Seq->AddBehaviour(new AssignEatTarget());

	Sequence* EatTarget_Seq= new Sequence();
	EatTarget_Seq->AddBehaviour(new IsHungriestMember());
	EatTarget_Seq->AddBehaviour(new CloseEnoughToEat());
	EatTarget_Seq->AddBehaviour(new ConsumeTarget());

	DeadTarget_Seq->AddBehaviour(EatTarget_Seq);


	// STACK ACCORDING TO CHART LAYOUT
	Attack_RemoveTarget_Sel->AddBehaviour(DeadTarget_Seq);
	Attack_RemoveTarget_Sel->AddBehaviour(Attack_Seq);
	Attack_RemoveTarget_Sel->AddBehaviour(RemovePrey_Seq);
	AttackOrRemovePrey_Seq->AddBehaviour(new DoesFlockHaveTarget());
	AttackOrRemovePrey_Seq->AddBehaviour(new ChaseAction());
	AttackOrRemovePrey_Seq->AddBehaviour(Attack_RemoveTarget_Sel);
	FindPrey_Seq->AddBehaviour(AttackOrRemovePrey_Seq);
	Hungry_Predator_Sel->AddBehaviour(FindPrey_Seq);
	Hungry_Predator_Sel->AddBehaviour(Reset_Hungry_Status_Seq);

	//AttackOrRemovePrey_Seq->AddBehaviour(RemovePrey_Seq);

	// Hunt Sequence
	//Sequence* Hunt_Seq = new Sequence();
	//Hunt_Seq->AddBehaviour(new DoesFlockHaveTarget());
	//Hunt_Seq->AddBehaviour(new ChaseAction());
	//Hunt_Seq->AddBehaviour(new CloseEnoughToAttack());
	//Hunt_Seq->AddBehaviour(new AttackTarget());

	// Wander Sequence
	/*Sequence* Wander_Seq = new Sequence();
	Wander_Seq->AddBehaviour(new InverseDecorator(new DoesFlockHaveTarget()));
	Wander_Seq->AddBehaviour(new WanderAction());*/

	// Add behaviours to tree base in order of execution
	Base_Sel->AddBehaviour(Hungry_Predator_Sel);			// 1. FindPrey Sequence
	//Base_Seq->AddBehaviour(Hunt_Seq);				// 3. Hunt Sequence
	//Base_Seq->AddBehaviour(Wander_Seq);			// 4. Wander Sequence

	BehaviourTree* m_BehaviourTree = new BehaviourTree(Base_Sel);


	AddBehaviour(m_BehaviourTree);

	
	// Flocking Forces
	//sb->AddSteeringForce(new FlockForces(m_Flock.GetFlockAgents()));
	//sb->AddSteeringForce(new SeperationForce(m_Flock.GetFlockAgents()));
	//sb->AddSteeringForce(new AlignmentForce(m_Flock.GetFlockAgents()));
	//sb->AddSteeringForce(new CohesionForce(m_Flock.GetFlockAgents()));


	//// Set Default Blackboard Values
	//GetBlackboardData("WanderSpeed").FloatData    = 40;
	//GetBlackboardData("WanderRadius").FloatData   = 50;
	//GetBlackboardData("WanderDistance").FloatData = 10;
	//GetBlackboardData("WanderWeight").FloatData   = 1.f;

	/*GetBlackboardData("SeperateWeight").FloatData  = .05f;
	GetBlackboardData("AlingmentWeight").FloatData = 2.f;
	GetBlackboardData("CohesionWeight").FloatData  = .7f;*/

	//GetBlackboardData("SeperateWeight").FloatData = 7.4f;
	//GetBlackboardData("AlingmentWeight").FloatData = 7.2f;
	//GetBlackboardData("CohesionWeight").FloatData = 7.f;

	GetBlackboardData("PackTarget").AgentData = nullptr;

	GetBlackboardData("MaxSeperation").FloatData = 140;
	GetBlackboardData("SeperationRadius").FloatData = 140;

	GetBlackboardData("SeekSpeed").FloatData = 500;
	m_Flock.GetFlockBlackboardData("SeekOn").BoolData = false;

}