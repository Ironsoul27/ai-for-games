#include "FSM_Condition.h"
#include "Agent.h"
#include <assert.h>


Condition::Condition()
{
}


Condition::~Condition()
{
}

WithinRangeCondition::WithinRangeCondition(const Agent * agent, float range)
	: m_Target(agent), m_RangeSqr(range * range)
{
}

bool WithinRangeCondition::Test(Agent * agent) const
{
	assert(m_Target != nullptr);

	Vector2 dir = m_Target->GetPosition();
	dir -= agent->GetPosition();

	//Vector2 dir2 = (m_Target->GetPosition() - agent->GetPosition());
	float magSqr = (dir.x * dir.x + dir.y * dir.y);
	return (magSqr < m_RangeSqr);
}




NotCondition::NotCondition(Condition * originalCondition)
	: m_Condition(originalCondition)
{
}

NotCondition::~NotCondition()
{
	if (m_Condition != nullptr)
		delete m_Condition;
}

bool NotCondition::Test(Agent * agent) const
{
	assert(m_Condition != nullptr);

	// Just return the opposite of what the original condition returns
	return !m_Condition->Test(agent);
}
