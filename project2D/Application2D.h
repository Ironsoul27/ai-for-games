#pragma once
#include "Application.h"
#include "Renderer2D.h"
#include "Agent.h"
#include "Flock.h"
#include "Pathfinding.h"
#include "Predator.h"
#include "Prey.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <memory>
#include <algorithm>
#include <sstream>
#include <list>
#include <forward_list>
#include <random>


using std::vector;
using std::unordered_map;
using std::string;

using namespace Pathfinding;

typedef std::unordered_map<std::string, Agent::BlackboardData> BlackBoard_Shared_Ptr;
typedef std::unordered_map<string, vector<Agent*>> Agent_UOM;
typedef std::unordered_map<string, vector<Flock>&> Flock_Map;

class Sphere;
class SceneObject;

class Application2D : public aie::Application {
public:

	Application2D() {}
	virtual ~Application2D() {}

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	std::default_random_engine generator;

	Vector2 FindSpawnLocation(const Sphere& spawnZone);
	Vector2 FindRandomSSSpawnLocation(Sphere& thisCollider, const vector<Sphere*>& avoidColliders);

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;

	std::shared_ptr<aie::Texture> Waterhole_tex;
	std::shared_ptr<aie::Texture> Tree_tex;


	Agent_UOM m_Agents;

	vector<Agent*> m_Prey;
	vector<Agent*> m_Predators;

	vector<Flock> m_PreyFlocks;
	vector<Flock> m_PredatorFlocks;

	Flock_Map m_AgentFlocks = {
		{ "PreyFlocks", m_PreyFlocks },
		{ "PredatorFlocks", m_PredatorFlocks }
	};

	Agent* m_Player;

	std::forward_list <std::string> m_PredatorWatchList;
	std::forward_list <std::string> m_PreyWatchList;

	vector<SceneObject*> m_SceneObjects;

	float m_timer;

	// The nodegraph used by agents to perform pathfinding in the world
	NodeGraph2D m_NodeGraph;
};

