#include "Prey.h"

#include "Application.h"
#include "KeyboardBehaviour.h"
#include "SteeringBehaviour.h"
#include "DebugBehaviour.h"
#include "DragBehaviour.h"
#include "WrapScreenBehaviour.h"
#include "Pathfinding.h"
#include "BehaviourTree.h"
#include "BT_Actions.h"
#include "BT_Conditions.h"
#include "FollowLeaderBehaviour.h"
#include "UpdateNecessities.h"
#include "Flock.h"

class Flock;


Prey::Prey(Flock& sharedFlock, const std::string& name, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects)
	: Animal(sharedFlock, name)
{
	Init(targetAgents, agentwatchlist, app, sceneObjects);
}

Prey::Prey(Flock& sharedFlock, const std::string & name, const Vector2 & pos, const Vector3 & col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects)
	: Animal(sharedFlock, name, pos, col)
{
	Init(targetAgents, agentwatchlist, app, sceneObjects);
}

Prey::Prey(Flock& sharedFlock, const std::string & name, const Vector2 & pos, const Vector2 & vel, const Vector3 & col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects)
	: Animal(sharedFlock, name, pos, vel, col)
{
	Init(targetAgents, agentwatchlist, app, sceneObjects);
}


Prey::~Prey()
{
}

const void Prey::Init(Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects)
{

	AddBehaviour(new UpdateNecessities());

	SteeringBehaviour* sb = new SteeringBehaviour();
	sb->AddSteeringForce(new CollisionAvoidance(sceneObjects));


	if (GetName().compare("Prey 1") == 0) 
	{
		m_Flock.SetFlockLeader(this);

		AddBehaviour(new FollowLeaderBehaviour()); // Needs to become global with method to change who is the leader later along with method to check if agent is the leader in the followleader and wander forces
		AddBehaviour(new DebugBehaviour());

		sb->AddSteeringForce(new WanderForce(app, sceneObjects));

		GetBlackboardData("WanderSpeed").FloatData = 400;
		GetBlackboardData("WanderRadius").FloatData = 100;
		GetBlackboardData("WanderDistance").FloatData = 20;
		m_StateData = Wander;
	}
	else if (!GetName().compare("Player") == 0)
	{
		sb->AddSteeringForce(new SeperationForce()); 
		sb->AddSteeringForce(new SeekFleeForce());

		Selector* Base_Sel = new Selector();

		Sequence* FleeSequence = new Sequence();
		FleeSequence->AddBehaviour(new InRangeCheck(targetAgents, agentwatchlist, 120));
		FleeSequence->AddBehaviour(new DoesFlockHaveTarget());
		FleeSequence->AddBehaviour(new FleeAction());

		Sequence* Remove_Target_Seq = new Sequence();
		Remove_Target_Seq->AddBehaviour(new IsLastMember());
		Remove_Target_Seq->AddBehaviour(new InverseDecorator(new AreFleeFlocksRemaining()));
		Remove_Target_Seq->AddBehaviour(new ClearFleeFlocks());
		//Remove_Target_Seq->AddBehaviour(new InverseDecorator(new AreFleeFlocksRemaining()));
		Remove_Target_Seq->AddBehaviour(new SetDefaultStates());

		FleeSequence->AddBehaviour(Remove_Target_Seq);

		Base_Sel->AddBehaviour(FleeSequence);		// 1. Flee Sequence
		BehaviourTree* m_BehaviourTree = new BehaviourTree(Base_Sel);
		AddBehaviour(m_BehaviourTree);

		m_StateData = FollowLeader;
	}

	AddBehaviour(sb);

	GetBlackboardData("FleeSpeed").FloatData = 10;
}
