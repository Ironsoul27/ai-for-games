#pragma once
#include "Animal.h"
#include "Flock.h"
#include <utility>
#include <unordered_map>
#include <algorithm>


namespace aie {
	class Application;
}

class SceneObject;

typedef std::pair<Flock, bool> Flock_Pair;

typedef std::unordered_map < std::string, Flock_Pair> Flee_Agents_Map;
typedef std::forward_list<std::string> Flee_List;
typedef std::forward_list <std::string> Agent_Watch_List;


class Prey :
	public Animal
{
public:

	typedef std::unordered_map<std::string, BlackboardData> BlackBoard_Map;

	Prey(Flock& sharedFlock, const std::string& name, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects);
	Prey(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector3& col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects);
	Prey(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector2& vel, const Vector3& col, Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects);
	virtual ~Prey();

	
	
	// Flee Flock list must be iterated over to provide suitable flee behaviour, when adding requires the id for the flock and flock reference. When removing we pass through the flocks id to be removed from the container

protected:

	const void Init(Agent_UOM& targetAgents, Agent_Watch_List& agentwatchlist, aie::Application* app, vector<SceneObject*>& sceneObjects);

	Flee_Agents_Map& m_FleeAgents = m_Flock.GetFleeAgentList();
};
