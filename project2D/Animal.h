#pragma once
#include "Agent.h"
#include <forward_list>

class Animal :
	public Agent
{
public:
	Animal(Flock& sharedFlock, const std::string name);
	Animal(Flock& sharedFlock, const std::string& name, const Vector2& pos, const Vector3& col);
	Animal(Flock& sharedFlock, const std::string & name, const Vector2 & pos, const Vector2 & vel, const Vector3 & col);
	virtual ~Animal();

	void Init();

	const float		GetHealth() { return m_Health; }
	const float		GetHunger() { return m_Hunger; }
	const float		GetThirst() { return m_Thirst; }
	const float		GetTiredness() { return m_Tiredness; }

	void			SetHealth(const float val);
	void			SetHunger(const float val);
	void			SetThirst(const float val);
	void			SetTiredness(const float val);

	const void		TakeDamage(float damage);

protected:

	float m_Health = 100;
	float m_Hunger = 19;
	float m_Thirst = 100;
	float m_Tiredness = 0;

private:
	float RandomFloat(float a, float b);
	void  Death();
};


