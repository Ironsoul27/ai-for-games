#include "DebugBehaviour.h"
#include "Agent.h"
#include <Renderer2D.h>
#include <Font.h>
#include <sstream>
#include <assert.h>
#include "Animal.h"
#include "Flock.h"


DebugBehaviour::DebugBehaviour()
{
}


DebugBehaviour::~DebugBehaviour()
{
}

bool DebugBehaviour::Update(Agent * agent, float deltaTime)
{
	return true;
}

void DebugBehaviour::Draw(Agent* agent, aie::Renderer2D * renderer)
{
	assert(renderer != nullptr && "DEBUG BEHAVIOUR -- Draw failed to pass through renderer");

	renderer->setRenderColour(0.7f, 0.7f, 0.7f);

	if (agent->BlackboardDataExists("DebugOn") && agent->GetBlackboardData("DebugOn").BoolData == true)
	{
		m_TextYPos = agent->GetPosition().y - 10;

		// If dead return after drawing dead sign
		if (agent->BlackboardDataExists("IsDead"))
			if (agent->GetBlackboardData("IsDead").BoolData) {
				drawText(agent, renderer, "DEAD X_X");
				return;
			}

		const Vector2& pos = agent->GetPosition();
		const Vector2& vel = agent->GetVelocity();

		// Draw velocity line
		renderer->drawLine(pos.x, pos.y, pos.x + vel.x, pos.y + vel.y);

		if (agent->GetName() != "")
			drawText(agent, renderer, agent->GetName());

		// Animal Stats
		Animal* animal = dynamic_cast<Animal*>(agent);
		if (animal != nullptr)
		{
			if (agent->GetFlock().IsLeader(agent))
			{
				drawText(agent, renderer, "Leader");
				if (agent->GetFlock().FlockBlackboardDataExists("LeaderFollowPoint"))
				{
					const Vector2& followPoint = agent->GetFlock().GetFlockBlackboardData("LeaderFollowPoint").Vector2Data;
					renderer->drawCircle(followPoint.x, followPoint.y, 10);

					/*if (agent->GetFlock().FlockBlackboardDataExists("HungriestMember"))
						drawText(agent, renderer, "Hungriest Member: " + agent->GetFlock().GetFlockBlackboardData("HungriestMember").AgentData->GetName());*/

					if (agent->GetFlock().FlockBlackboardDataExists("CanStillSeeTarget"))
						drawBool(agent, renderer, "Can See Target: ", agent->GetFlock().GetFlockBlackboardData("CanStillSeeTarget").BoolData);

					if (agent->GetFlock().FlockBlackboardDataExists("PackTarget"))
						drawBool(agent, renderer, "Is Target Set: ", agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData == nullptr);

					//if (agent->GetFlock().FlockBlackboardDataExists())
				}			
			}

			drawFloat(agent, renderer, "Health", animal->GetHealth());
			drawFloat(agent, renderer, "Hunger", animal->GetHunger());
			drawFloat(agent, renderer, "Thirst", animal->GetThirst());
			drawFloat(agent, renderer, "Tiredness", animal->GetTiredness());
		}

		drawText(agent, renderer, agent->GetFlockId());

		if (agent->BlackboardDataExists("Drag"))
			drawBlackboardFloat(agent, renderer, "Drag");

		if (agent->BlackboardDataExists("CollisionPoints"))
		{
			drawBlackboardRay(agent, renderer, "CollisionPoints");

			const Vector2& temp = agent->GetBlackboardData("CollisionPoints").Vector2Data;
			renderer->drawCircle(temp.x, temp.y, 5);
		}
			
			
		/*if (agent->BlackboardDataExists("SeekTarget") && agent->BlackboardDataExists("SeekSpeed"))
		{
			drawText(agent, renderer, "SeekTarget: " + agent->GetBlackboardData("SeekTarget").AgentData->GetName());
			drawBlackboardFloat(agent, renderer, "SeekSpeed");
		}*/

		if (agent->BlackboardDataExists("FleeTarget") && agent->BlackboardDataExists("FleeSpeed"))
		{
			drawText(agent, renderer, "FleeTarget: " + agent->GetBlackboardData("FleeTarget").AgentData->GetName());
			drawBlackboardFloat(agent, renderer, "FleeSpeed");
		}

		if (agent->BlackboardDataExists("WanderTarget")/* && agent->BlackboardDataExists("WanderSpeed")*/)
		{
			const Vector2& temp = agent->GetBlackboardData("WanderTarget").Vector2Data;
			renderer->drawCircle(temp.x, temp.y, 5);
			//drawBlackboardVector2(agent, renderer, "WanderTarget");
			//drawBlackboardFloat(agent, renderer, "WanderSpeed");
		}
	}
}

void DebugBehaviour::drawText(Agent * agent, aie::Renderer2D * renderer, const std::string & text)
{
	renderer->drawText(g_SystemFont, text.c_str(), agent->GetPosition().x + 10, m_TextYPos);
	m_TextYPos -= g_SystemFont->getStringHeight(text.c_str());
}

void DebugBehaviour::drawBlackboardFloat(Agent * agent, aie::Renderer2D * renderer, const std::string & blackboardId)
{
	std::stringstream s;
	s << blackboardId << ":" << agent->GetBlackboardData(blackboardId).FloatData;
	renderer->drawText(g_SystemFont, s.str().c_str(), agent->GetPosition().x + 10, m_TextYPos);
	m_TextYPos -= g_SystemFont->getStringHeight(s.str().c_str());
}

void DebugBehaviour::drawBlackboardVector2(Agent * agent, aie::Renderer2D * renderer, const std::string & blackboardId)
{
	std::stringstream s;
	Vector2 v = agent->GetBlackboardData(blackboardId).Vector2Data;
	s << blackboardId << ":" << v.x << "," << v.y;
	renderer->drawText(g_SystemFont, s.str().c_str(), agent->GetPosition().x + 10, m_TextYPos);
	m_TextYPos -= g_SystemFont->getStringHeight(s.str().c_str());
}

void DebugBehaviour::drawBlackboardRay(Agent * agent, aie::Renderer2D * renderer, const std::string & blackboardId)
{
	Ray& temp = agent->GetBlackboardData(blackboardId).RayData;
	const Vector2& endPoint = temp.origin + temp.direction.normalised() * temp.direction.magnitude();

	renderer->setRenderColour(1, .3f, 1);
	renderer->drawLine(temp.origin.x, temp.origin.y, endPoint.x, endPoint.y);
	renderer->setRenderColour(1, 1, 1);
}

void DebugBehaviour::drawFloat(Agent * agent, aie::Renderer2D* renderer, const std::string & prefix, const float data)
{
	std::stringstream s;
	s << prefix << ":" << data;
	renderer->drawText(g_SystemFont, s.str().c_str(), agent->GetPosition().x + 10, m_TextYPos);
	m_TextYPos -= g_SystemFont->getStringHeight(s.str().c_str());
}

void DebugBehaviour::drawBool(Agent * agent, aie::Renderer2D * renderer, const std::string & prefix, bool data)
{
	std::stringstream s;
	s << prefix << ": ";
	data == true ? s << "True" : s << "False";
	renderer->drawText(g_SystemFont, s.str().c_str(), agent->GetPosition().x + 10, m_TextYPos);
	m_TextYPos -= g_SystemFont->getStringHeight(s.str().c_str());
}
