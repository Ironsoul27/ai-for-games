#include "BT_Conditions.h"
#include "Agent.h"
#include <assert.h>
#include "Flock.h"
#include "Predator.h"
#include "Prey.h"
#include "Animal.h"


BehaviourResult IsHungry::execute(Agent * agent, float deltaTime)
{
	Animal* animal = dynamic_cast<Animal*>(agent);
	if (animal != nullptr)
	{
		if (animal->GetHunger() < 20)
			return Success; // When making the action to change blackboard values make the flock bool 'ismemberhungry' to true and mark 'HungryMember' to this agent,
							// The flock may not need to communicate on the blackboard since they can all access they're own flock, so a list of hungry members should be included in the flock class
		else
			return Failure;
	}


	//assert(animal != nullptr && "Agent was none of the derived classes");
	return Failure;
}


BehaviourResult IsThirsty::execute(Agent * agent, float deltaTime)
{
	Animal* animal = dynamic_cast<Animal*>(agent);
	if (animal != nullptr)
	{
		if (animal->GetThirst() < 20)
			return Success;
		else
			return Failure;
	}

	return Failure;
}


BehaviourResult IsFlockMemberHungry::execute(Agent * agent, float deltaTime)
{
	// Change to check members flock instead  // or keep this for a simple check if one member is hungry  // Maybe make it a container that keeps it's contents ordered automatically, or work out hungriest member when hunger is updated
	if (agent->GetFlock().IsFLockMemberHungry())
		return Success;

	return Failure;
}

BehaviourResult IsLastMember::execute(Agent * agent, float deltaTime)
{
	if (agent == agent->GetFlock().GetFlockAgents().back())
		return Success;
	
	return Failure;

}

BehaviourResult CanFlockNotSeeTarget::execute(Agent * agent, float deltaTime) 
{
	if (agent->GetFlock().FlockBlackboardDataExists("CanStillSeeTarget") && 
		agent->GetFlock().GetFlockBlackboardData("CanStillSeeTarget").BoolData == false)
	{		
		return Success;
	}
	
	// Reset the flocks canstillseetarget bool
	agent->GetFlock().GetFlockBlackboardData("CanStillSeeTarget").BoolData = false;
	return Failure;
}

BehaviourResult DoesFlockHaveTarget::execute(Agent * agent, float deltaTime)
{
	Prey* prey = dynamic_cast<Prey*>(agent);
	if (prey != nullptr) {
		if (prey->GetFlock().GetFleeFlocks().size() != 0)
			return Success;
	}

	Predator* predator = dynamic_cast<Predator*>(agent);
	if (predator != nullptr) {
		if (agent->GetFlock().FlockBlackboardDataExists("PackTarget"))
			return Success;
	}
	
		return Failure;
}


BehaviourResult CloseEnoughToAttack::execute(Agent * agent, float deltaTime)
{
	Agent* target = nullptr;
	float attackDistance = 100;

	if (agent->BlackboardDataExists("AttackDistance"))
		attackDistance = agent->GetBlackboardData("AttackDistance").FloatData;

	if (agent->BlackboardDataExists("PackTarget"))
		target = agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData;

	if (target == nullptr)
		return Failure;

	Vector2 dir = target->GetPosition() - agent->GetPosition();
	float magSqr = dir.magnitudeSqr();

	if (magSqr < attackDistance)
		return Success;
	else
		return Failure;

}


BehaviourResult IsTargetDead::execute(Agent * agent, float deltaTime)
{
	/*if (!agent->BlackboardDataExists("PackTarget"))
		return Success;*/

	Animal* animal = dynamic_cast<Animal*>(agent->GetFlock().GetFlockBlackboardData("PackTarget").AgentData);
	if (animal != nullptr)
	{
		if (animal->GetHealth() <= 0)
			return Success;
		else
			return Failure;
	}
	//assert(animal != nullptr && "IsTargetDead Cond -- Flock Target was unable to cast to animal");
	return Failure;
}


BehaviourResult IsHungriestMember::execute(Agent * agent, float deltaTime)
{
	if (agent == agent->GetFlock().GetFlockBlackboardData("HungriestMember").AgentData) // Change if neccessary // maybe GetHungriestMember
		return Success;
	else
		return Failure;
}


BehaviourResult CloseEnoughToEat::execute(Agent * agent, float deltaTime)
{
	Agent* hungriestMember = agent->GetFlock().GetFlockBlackboardData("HungriestMember").AgentData;
	
	Vector2 dir = hungriestMember->GetPosition() - agent->GetPosition();
	float magSqr = dir.magnitudeSqr();

	if (magSqr < 50)
		return Success;
	else
		return Failure;
}

BehaviourResult AreFleeFlocksRemaining::execute(Agent * agent, float deltaTime)
{
	Prey* prey = dynamic_cast<Prey*>(agent);
	if (prey != nullptr)
	{
		if (prey->GetFlock().GetFleeFlocks().size() != 0)
			return Success;
	}
	assert(prey != nullptr);

	return Failure;
}
