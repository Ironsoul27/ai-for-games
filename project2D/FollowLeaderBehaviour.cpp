#include "FollowLeaderBehaviour.h"
#include "Agent.h"
#include "Flock.h"
#include <Vector2.h>


FollowLeaderBehaviour::FollowLeaderBehaviour()
{
}


FollowLeaderBehaviour::~FollowLeaderBehaviour()
{
}

bool FollowLeaderBehaviour::Update(Agent * agent, float deltaTime) // Should be attached to leader and accessed through flock blackboard
{
	Vector2 leaderFollowPos;
	float   leaderBehindDistance = 10;

	if (agent->BlackboardDataExists("LeaderBehindDistance"))
		leaderBehindDistance = agent->GetBlackboardData("LeaderBehindDistance").FloatData;

	leaderFollowPos = agent->GetVelocity();
	leaderFollowPos = leaderFollowPos * -1;
	leaderFollowPos.normalise();
	leaderFollowPos = leaderFollowPos * leaderBehindDistance;
	leaderFollowPos = leaderFollowPos + agent->GetPosition();

	agent->GetFlock().GetFlockBlackboardData("LeaderFollowPoint").Vector2Data = leaderFollowPos;

	return true;
}
