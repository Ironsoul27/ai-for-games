#include "stdafx.h"
#include "Matrix4.h"


Matrix4::Matrix4()
{
}

Matrix4::Matrix4(float x_x, float x_y, float x_z, float x_w,
	float y_x, float y_y, float y_z, float y_w,
	float z_x, float z_y, float z_z, float z_w,
	float w_x, float w_y, float w_z, float w_w) {

	data[0][0] = x_x, data[0][1] = x_y, data[0][2] = x_z, data[0][3] = x_w,
		data[1][0] = y_x, data[1][1] = y_y, data[1][2] = y_z, data[1][3] = y_w,
		data[2][0] = z_x, data[2][1] = z_y, data[2][2] = z_z, data[2][3] = z_w,
		data[3][0] = w_x, data[3][1] = w_y, data[3][2] = w_z, data[3][3] = w_w;
};


Matrix4::~Matrix4()
{
}

const Matrix4 Matrix4::identity = Matrix4(1, 0, 0, 0,
										  0, 1, 0, 0,
										  0, 0, 1, 0,
										  0, 0, 0, 1);

Vector4& Matrix4::operator[] (int index) {
	return Matrix4::axis[index];
}

const Vector4& Matrix4::operator[] (int index) const {
	return axis[index];
}

Matrix4& Matrix4::operator= (const Matrix4& other) {
	xAxis = other.xAxis;
	yAxis = other.yAxis;
	zAxis = other.zAxis;
	translation = other.translation;

	return *this;
}

void Matrix4::setScaled(float x, float y, float z) {

	xAxis = { x, 0, 0, 0 };
	yAxis = { 0, y, 0, 0 };
	zAxis = { 0, 0, z, 0 };
	translation = { 0, 0, 0, 1 };
}

void Matrix4::setRotateX(float radians) { 

	xAxis = { 1, 0, 0, 0 };
	yAxis = { 0, cosf(radians), sinf(radians), 0 };
	zAxis = { 0, -sinf(radians), cosf(radians), 0 };
	translation = { 0, 0, 0, 1 };
}

void Matrix4::setRotateY(float radians) {

	xAxis = { cosf(radians), 0, -sinf(radians), 0 };
	yAxis = { 0, 1, 0, 0 };
	zAxis = { sinf(radians), 0, cosf(radians), 0 };
	translation = { 0, 0, 0, 1 };
}

void Matrix4::setRotateZ(float radians) {

	xAxis = { cosf(radians), sinf(radians), 0 };
	yAxis = { -sinf(radians), cosf(radians), 0 };
	zAxis = { 0, 0, 1, 0 };
	translation = { 0, 0, 0, 1 }; 
}

void Matrix4::rotateX(float radians) {
	Matrix4 m;
	m.setRotateX(radians);

	*this = *this * m;
}

void Matrix4::rotateY(float radians) {
	Matrix4 m;
	m.setRotateY(radians);

	*this = *this * m;
}

void Matrix4::rotateZ(float radians) {
	Matrix4 m;
	m.setRotateZ(radians);

	*this = *this * m;
}

void Matrix4::setEuler(float pitch, float yaw, float roll) {
	Matrix4 x, y, z;

	x.setRotateX(pitch);
	y.setRotateY(yaw);
	z.setRotateZ(roll);

	*this = z * y * x;

}


void Matrix4::translate(float x, float y, float z) {
	translation += Vector4(x, y, z, 0);
}

Matrix4 Matrix4::operator* (const Matrix4& other) {

	Matrix4 result;

	for (int r = 0; r < 4; ++r) {
		for (int c = 0; c < 4; ++c) {
			result.data[c][r] = data[0][r] * other.data[c][0] +
				data[1][r] * other.data[c][1] +
				data[2][r] * other.data[c][2] +
				data[3][r] * other.data[c][3];

		}
	}

	return result;
}

Vector4 Matrix4::operator* (const Vector4& v) const {

	Vector4 result;

	result[0] = data[0][0] * v[0] + data[1][0] * v[1] +
		data[2][0] * v[2] + data[3][0] * v[3];

	result[1] = data[0][1] * v[0] + data[1][1] * v[1] +
		data[2][1] * v[2] + data[3][1] * v[3];

	result[2] = data[0][2] * v[0] + data[1][2] * v[1] +
		data[2][2] * v[2] + data[3][2] * v[3];

	result[3] = data[0][3] * v[0] + data[1][3] * v[1] +
		data[2][3] * v[2] + data[3][3] * v[3];

	return result;
}

Matrix4::operator float* () { return m; }
Matrix4::operator const float* () const { return m; } 