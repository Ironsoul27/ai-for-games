#include "stdafx.h"
#include "Sphere.h"
#include "AABB.h"


void Sphere::fit(const Vector2 * points, unsigned int count)
{
}

void Sphere::fit(const std::vector<Vector2>& points)
{
}

bool Sphere::overlaps(const Vector2 & p) const
{
	Vector2 toPoint = p - center;
	return toPoint.magnitudeSqr() <= (radius * radius);
}

bool Sphere::overlaps(const Sphere & other) const
{
	Vector2 diff = other.center - center;

	float r = radius + other.radius;
	return diff.magnitudeSqr() <= (r * r);
}

bool Sphere::overlaps(AABB & aabb) const
{
	auto diff = aabb.closestPoint(center) - center;
	return diff.dot(diff) <= (radius * radius);
}

Vector2 Sphere::closestPoint(const Vector2 & p) const
{
	Vector2 toPoint = p - center;

	if (toPoint.magnitudeSqr() > (radius * radius))
		toPoint = toPoint.normalised() * radius;

	return (center + toPoint);
}
