#pragma once
#include "Vector4.h"
#include "Vector3.h"
#include "Matrix3.h"
class Matrix4
{
public:
	Matrix4();
	Matrix4(float x_x, float x_y, float x_z, float x_w,
			float y_x, float y_y, float y_z, float y_w,
			float z_x, float z_y, float z_z, float z_w,
			float w_x, float w_y, float w_z, float w_w);

	~Matrix4();


	Vector4& operator[] (int index);
	const Vector4& operator[] (int index) const;

	Matrix4& operator= (const Matrix4& other);

	void setScaled(float x, float y, float z);
	void setRotateX(float radians);
	void setRotateY(float radians);
	void setRotateZ(float radians);
	void rotateX(float radians);
	void rotateY(float radians);
	void rotateZ(float radians);
	void setEuler(float pitch, float yaw, float roll);
	void translate(float x, float y, float z);

	Matrix4 operator* (const Matrix4& other);
	Vector4 operator* (const Vector4& v) const;

	operator float* ();
	operator const float* () const; 

public:
	union {
		struct {
			Vector4 xAxis;
			Vector4 yAxis;
			Vector4 zAxis;
			Vector4 translation;
		};
		Vector4 axis[4];
		float data[4][4];
		float m[16];

	};

	static const Matrix4 identity;
};