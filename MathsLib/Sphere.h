#pragma once
#include "Vector2.h"
#include <vector>

class AABB;

class Sphere
{
public:
	Sphere() {}
	Sphere(const Vector2& p, float r)
		: center(p), radius(r) {}
	~Sphere() {}

	void fit(const Vector2* points, unsigned int count);
	void fit(const std::vector<Vector2>& points);
	bool overlaps(const Vector2& p) const;
	bool overlaps(const Sphere& other) const;
	bool overlaps(AABB& aabb) const;
	Vector2 closestPoint(const Vector2& p) const;

	Vector2 center;
	float radius;
};

