#include "stdafx.h"
#include "Ray.h"
#include "AABB.h"
#include "Sphere.h"

//Vector2 Ray::closestPoint(const Vector2 & point) const
//{
//	auto p = point - origin;
//
//	float t = clamp(p.dot(direction), 0, length); // find suitable clamp
//
//	return origin + direction * t;
//}

bool Ray::intersects(const AABB & aabb, Vector2 * I) const
{
	float xmin, xmax, ymin, ymax;

	if (direction.x < 0) {
		xmin = (aabb.max.x - origin.x) / direction.x;
		xmin = (aabb.min.x - origin.x) / direction.x;
	}
	else {
		xmin = (aabb.min.x - origin.x) / direction.x;
		xmax = (aabb.max.x - origin.x) / direction.x;
	}

	if (direction.y < 0) {
		ymin = (aabb.max.y - origin.y) / direction.y;
		ymin = (aabb.min.y - origin.y) / direction.y;
	}
	else {
		ymin = (aabb.min.y - origin.y) / direction.y;
		ymax = (aabb.max.y - origin.y) / direction.y;
	}

	// Ensure within box
	if (xmin > ymax || ymin > xmax)
		return false;

	float t = max(xmin, ymin);

	// Intersects if within range
	if (t >= 0 && t <= length) {

		//  Store intersection points if requested
		if (I != nullptr)
			*I = origin + direction * t;

		return true;
	}

	// Default for no intersection
	return false;
}

bool Ray::intersects(const Sphere & sphere, Vector2 * I) const
{
	// Ray origin to sphere center
	auto L = sphere.center - origin;

	// Project sphere center onto ray
	float t = L.dot(direction);

	// Get sqr distance from sphere center to ray
	float dd = L.dot(L) - t * t;

	// Subtract penetration amount from projected distance
	t -= sqrt(sphere.radius * sphere.radius - dd);

	// Intersects if within range length
	if (t >= 0 && t <= length) {

		//  Store intersection points if requested
		if (I != nullptr)
			*I = origin + direction * t;
		return true;
	}

	// Default for no intersection
	return false;
}
