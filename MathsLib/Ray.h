#pragma once
#include "Vector2.h"
#include "Sphere.h"

class AABB;

class Ray
{
public:
	Ray() {}
	Ray(const Vector2& start, const Vector2& dir, float l = INFINITY)
		: origin(start), direction(dir), length(l) {}
	~Ray() {}

	//Vector2 closestPoint(const Vector2& point) const;
	bool intersects(const AABB& aabb, Vector2* I = nullptr) const;
	bool intersects(const Sphere& sphere, Vector2* I = nullptr) const;


	Vector2 origin;
	Vector2 direction;
	float length;

};

