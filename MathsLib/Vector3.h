#pragma once
#include <cmath>
#include "Vector2.h"
class Vector3
{
public:
	Vector3();
	Vector3(float x, float y, float z);
	~Vector3();

public:
	float operator[] (int index) const;
	float& operator[] (int index);

	Vector3 operator+ (const Vector3& other) const;
	Vector3 operator- (float scalar) const;
	Vector3 operator- (const Vector3& other) const;
	Vector3& operator- (const Vector3& other);
	Vector3 operator* (float scalar) const;
	Vector3 operator* (const Vector3& other) const;
	Vector3 operator* (const Vector2& other) const;
	Vector3 operator/ (float scalar) const;

	Vector3& operator= (const Vector3& other);
	Vector3& operator+= (const Vector3& other);
	Vector3& operator-= (const Vector3& other);
	Vector3& operator*= (const Vector3& other);
	Vector3& operator/= (const Vector3& other);

	
	float magnitudeSqr() const;
	float distance(const Vector3& other) const;
	float distanceSqr(const Vector3& other) const;
	void normalise();
	Vector3 normalised() const;
	float magnitude() const;
	float dot(const Vector3& other) const;
	Vector3 cross(const Vector3& other) const;
	bool withinRange(const Vector3& other, float range) const;

	const Vector3 lerp(const Vector3& a, const Vector3& b, float t) const;
	Vector3 getPerpendicularRH() const;
	float angleBetween(const Vector3& other) const;

	operator float* ();
	operator const float* () const;

public:
	union
	{
		struct {
			float x, y;
			union {
				float z, w;
			};
		};

		float data[3];
	};
};

Vector3 operator* (float scalar, Vector3& v);