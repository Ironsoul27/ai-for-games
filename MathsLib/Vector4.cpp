#include "stdafx.h"
#include "Vector4.h"


Vector4::Vector4()
{
	x = y = z = w = 0;
}

Vector4::Vector4(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

Vector4::~Vector4()
{
}

Vector4 operator* (float scalar, Vector4& v) {     
	return { v * scalar };
}

float Vector4::operator[] (int index) const { return data[index]; }
float& Vector4::operator[] (int index) { return data[index]; }

Vector4 Vector4::operator+ (const Vector4& other) const {
	return { x + other.x, y + other.y, z + other.z, w + other.w }; 
}

Vector4 Vector4::operator- (float scalar) const {
	return { x - scalar, y - scalar, z - scalar, w - scalar };
}

Vector4 Vector4::operator* (float scalar) const {
	return { x * scalar, y * scalar, z * scalar, w * scalar };
}

Vector4 Vector4::operator/ (float scalar) {
	return { x / scalar, y / scalar, z / scalar, w / scalar };
}

Vector4& Vector4::operator= (const Vector4& other) {
	x = other.x; y = other.y; z = other.z; w = other.w;
	return *this;
}

Vector4& Vector4::operator+= (const Vector4& other) {
	x += other.x; y += other.y; z += other.z; w += other.w;
	return *this;
}

Vector4& Vector4::operator-= (const Vector4& other) {
	x -= other.x; y -= other.y; z -= other.z; w -= other.w;
	return *this;
}

Vector4& Vector4::operator*= (const Vector4& other) {
	x *= other.x; y *= other.y; z *= other.z; w *= other.w;
	return *this;
}

Vector4& Vector4::operator/= (const Vector4& other) {
	x /= other.x; y /= other.y; z /= other.z; w /= other.w;
	return *this;
}

Vector4& Vector4::operator *=  (float scalar) {
	x *= scalar;
	y *= scalar;
	z *= scalar;
	w *= scalar;
	return *this;
}

Vector4& Vector4::operator- (const Vector4& other) {
	x = x - other.x;
	y = y - other.y;
	z = z - other.z;
	w = w - other.w;
	return *this;
}

float Vector4::magnitude() const {
	return sqrt(x * x + y * y + z * z + w * w);
}

void Vector4::normalise() {
	float mag = sqrt(x * x + y * y + z * z + w * w);
	x /= mag;
	y /= mag;
	z /= mag;
	w /= mag;

}

float Vector4::dot(const Vector4& other) const {
	return x * other.x + y * other.y + z * other.z + w * other.w;
}

Vector4 Vector4::cross(const Vector4& other) const {
	return { y * other.z - z * other.y,
		z * other.x - x * other.z,
		x * other.y - y * other.x,
		0 };
}

Vector4& Vector4::operator= (const Vector3& other) {
	x = other.x;
	y = other.y;
	z = other.z;
	w = other.w;
	return *this;
}

Vector4::operator float* () { return &x; }
Vector4::operator const float* () const { return &x; }