#include "stdafx.h"
#include "Vector3.h"


Vector3::Vector3()
{
	x = y = z = 0;
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
};

Vector3::~Vector3()
{
}

Vector3 operator* (float scalar, Vector3& v) {        
	return { v * scalar};
}


float Vector3::operator[] (int index) const { return data[index]; }
float& Vector3::operator[] (int index) { return data[index]; }

Vector3 Vector3::operator+ (const Vector3& other) const {
	return { x + other.x, y + other.y, z + other.z };
}

Vector3 Vector3::operator- (float scalar) const {
	return { x - scalar, y - scalar, z - scalar };
}

Vector3 Vector3::operator- (const Vector3& other) const {
	return { x - other.x, y - other.y, z - other.z };
}

Vector3 Vector3::operator* (float scalar) const {
	return { x * scalar, y * scalar, z * scalar };
}

Vector3 Vector3::operator* (const Vector3& other) const {
	return { x * other.x, y * other.y, z * other.z };
}

Vector3 Vector3::operator* (const Vector2& other) const {
	return { x * other.x, y * other.y, z };
}

Vector3 Vector3::operator/ (float scalar) const {
	return { x / scalar, y / scalar, z / scalar };
}

Vector3& Vector3::operator= (const Vector3& other) {
	x = other.x; y = other.y; z = other.z;
	return *this;
}

Vector3& Vector3::operator+= (const Vector3& other) {
	x += other.x; y += other.y; z += other.z;
	return *this;
}

Vector3& Vector3::operator-= (const Vector3& other) {
	x -= other.x; y -= other.y; z -= other.z;
	return *this;
}

Vector3& Vector3::operator*= (const Vector3& other) {
	x *= other.x; y *= other.y; z *= other.z;
	return *this;
}

Vector3& Vector3::operator/= (const Vector3& other) {
	x /= other.x; y /= other.y; z /= other.z;
	return *this;
}

Vector3& Vector3::operator- (const Vector3& other) {
	x = x - other.x;
	y = y - other.y;
	z = z - other.z;
	return *this;
}

//float magnitude() const { return sqrt(x * x + y * y + z * z); } // slower but more accurate

float Vector3::magnitudeSqr() const { return (x * x + y * y + z * z); }


float Vector3::distance(const Vector3& other) const {
	float diffx = x - other.x;
	float diffy = y - other.y;
	float diffz = z - other.z;
	return sqrt(diffx * diffx + diffy * diffy + diffz * diffz);
}

float Vector3::distanceSqr(const Vector3& other) const {
	float diffx = x - other.x;
	float diffy = y - other.y;
	float diffz = z - other.z;
	return (diffx * diffx + diffy * diffy + diffz * diffz);
}

void Vector3::normalise() {
	float mag = sqrt(x * x + y * y + z * z);
	x /= mag;
	y /= mag;
	z /= mag;
}

Vector3 Vector3::normalised() const {
	float mag = sqrt(x * x + y * y + z * z);
	return { x / mag, y / mag, z / mag };
}

float Vector3::magnitude() const {
	return sqrt(x * x + y * y + z * z);
}

float Vector3::dot(const Vector3& other) const {
	return x * other.x + y * other.y + z * other.z;
}

Vector3 Vector3::cross(const Vector3& other) const {
	return { y * other.z - z * other.y,
		z * other.x - x * other.z,
		x * other.y - y * other.x
	};
}

bool Vector3::withinRange(const Vector3 & other, float range = 0.1f) const
{
	return (x >= (other.x - range) && x <= (other.x + range)
		|| (y >= other.y - range) && (y <= other.y + range));
}

const Vector3 Vector3::lerp(const Vector3& a, const Vector3& b, float t) const {
	{
		return a * (1 - t) + b * t;
	};
}

Vector3 Vector3::getPerpendicularRH() const {
	return { -y, x, z };
}

float Vector3::angleBetween(const Vector3& other) const {
	// normalise the vectors
	Vector3 a = normalised();
	Vector3 b = other.normalised();
	// calculate the dot product
	float d = a.x * b.x + a.y * b.y + a.z * b.z;
	// return the angle between them
	return acos(d);
}


Vector3::operator float* () { return &x; }
Vector3::operator const float* () const { return &x; }
