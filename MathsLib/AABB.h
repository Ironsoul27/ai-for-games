#pragma once
#include "Vector2.h"
#include <vector>
#include <algorithm>

using std::min;
using std::max;

class AABB
{
public:
	AABB() {}
	AABB(const Vector2& min, const Vector2& max)
		: min(min), max(max) {}
		
	~AABB();


	Vector2 center() const;
	Vector2 extents() const;
	std::vector<Vector2> corners() const;
	void fit(const Vector2* points, unsigned int count);
	void fit(const std::vector<Vector2>& points);
	bool overlaps(const Vector2& p) const;
	bool overlaps(const AABB& other) const;
	Vector2 closestPoint(const Vector2 & p);

	template <typename T>
	T clamp(const T& n, const T& lower, const T& upper) {
		return std::max(lower, std::min(n, upper));
	}

	Vector2 min, max;
};